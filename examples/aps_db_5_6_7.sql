##############

### AP 4

#1
SELECT nombre, TIMESTAMPDIFF(YEAR, fecha_nacimiento, NOW()) as edad
FROM jugadores
WHERE nacionalidad = 'chilena'
  AND TIMESTAMPDIFF(YEAR, fecha_nacimiento, NOW()) > 25;

#2
SELECT j.nombre
FROM jugadores_equipos
         JOIN equipos e on e.id_equipo = jugadores_equipos.id_equipo
         JOIN jugadores j on jugadores_equipos.id_jugador = j.id_jugador
WHERE e.liga = 'la más bacán'
   OR (YEAR(fecha_nacimiento) = '1997' AND DAY(fecha_nacimiento) = '22');

#3
SELECT nombre,
       nacionalidad,
       TIMESTAMPDIFF(YEAR, fecha_nacimiento, NOW())       as edad,
       SQRT(TIMESTAMPDIFF(YEAR, fecha_nacimiento, NOW())) as raiz_cuadrada
FROM jugadores
WHERE nacionalidad = 'japonesa'
  AND TIMESTAMPDIFF(YEAR, fecha_nacimiento, NOW()) = (SELECT TIMESTAMPDIFF(YEAR, fecha_nacimiento, NOW()) as edad
                                                      FROM jugadores
                                                      WHERE nacionalidad = 'japonesa'
                                                      ORDER BY edad DESC
                                                      LIMIT 1)
ORDER BY edad DESC;

#4
SELECT j.nombre
FROM jugadores_equipos
         JOIN equipos e on e.id_equipo = jugadores_equipos.id_equipo
         JOIN jugadores j on jugadores_equipos.id_jugador = j.id_jugador
         JOIN jugadores_citados jc on j.id_jugador = jc.id_jugador
WHERE e.nombre = 'Karasuno'
  AND j.especialidad = 'rolling thunder'
  AND LENGTH(j.nombre > 5);

#5
SELECT j.nombre, COUNT(*) as conteo
FROM jugadores_citados
         JOIN jugadores j on j.id_jugador = jugadores_citados.id_jugador
         JOIN partidos p on p.id_partido = jugadores_citados.id_partido
         JOIN equipos e on p.equipo_local = e.id_equipo
         JOIN equipos e2 on e2.id_equipo = p.equipo_visita
         JOIN estadios e3 on p.id_estadio = e3.id_estadio
WHERE (e.nombre = 'Karasuno'
    OR e2.nombre = 'Karasuno')
  AND e3.direccion LIKE '%Tokyo%'
GROUP BY j.id_jugador
HAVING (SELECT COUNT(*) as conteo
        FROM jugadores_citados
                 JOIN jugadores j on j.id_jugador = jugadores_citados.id_jugador
                 JOIN partidos p on p.id_partido = jugadores_citados.id_partido
                 JOIN equipos e on p.equipo_local = e.id_equipo
                 JOIN equipos e2 on e2.id_equipo = p.equipo_visita
                 JOIN estadios e3 on p.id_estadio = e3.id_estadio
        WHERE (e.nombre = 'Karasuno'
            OR e2.nombre = 'Karasuno')
          AND e3.direccion LIKE '%Tokyo%'
        GROUP BY j.id_jugador
        ORDER BY conteo DESC
        LIMIT 1) = conteo
ORDER BY conteo DESC;

#6
SELECT jc.id_jugador, CONCAT(j2.nombre, ' (Hinata-friend)') as nombre, e.nombre as nombre_equipo
FROM jugadores_citados jc
         JOIN jugadores j2 on jc.id_jugador = j2.id_jugador
         JOIN jugadores_equipos je on j2.id_jugador = je.id_jugador
         JOIN equipos e on je.id_equipo = e.id_equipo
WHERE e.nombre = 'Karasuno'
  AND jc.id_partido IN (SELECT id_partido
                        FROM jugadores_citados
                        WHERE id_jugador = (SELECT id_jugador
                                            FROM jugadores j
                                            WHERE j.nombre like '%Hinata%'))
  AND jc.id_jugador != (SELECT id_jugador
                        FROM jugadores j
                        WHERE j.nombre like '%Hinata%')
GROUP BY jc.id_jugador;


#AP 5

#1
create
    definer = root@localhost function QUITAR_TILDES(str text) returns text deterministic no sql
BEGIN

    SET str = REPLACE(str, 'Š', 'S');
    SET str = REPLACE(str, 'š', 's');
    SET str = REPLACE(str, 'Ð', 'Dj');
    SET str = REPLACE(str, 'Ž', 'Z');
    SET str = REPLACE(str, 'ž', 'z');
    SET str = REPLACE(str, 'À', 'A');
    SET str = REPLACE(str, 'Á', 'A');
    SET str = REPLACE(str, 'Â', 'A');
    SET str = REPLACE(str, 'Ã', 'A');
    SET str = REPLACE(str, 'Ä', 'A');
    SET str = REPLACE(str, 'Å', 'A');
    SET str = REPLACE(str, 'Æ', 'A');
    SET str = REPLACE(str, 'Ç', 'C');
    SET str = REPLACE(str, 'È', 'E');
    SET str = REPLACE(str, 'É', 'E');
    SET str = REPLACE(str, 'Ê', 'E');
    SET str = REPLACE(str, 'Ë', 'E');
    SET str = REPLACE(str, 'Ì', 'I');
    SET str = REPLACE(str, 'Í', 'I');
    SET str = REPLACE(str, 'Î', 'I');
    SET str = REPLACE(str, 'Ï', 'I');
    SET str = REPLACE(str, 'Ñ', 'N');
    SET str = REPLACE(str, 'Ò', 'O');
    SET str = REPLACE(str, 'Ó', 'O');
    SET str = REPLACE(str, 'Ô', 'O');
    SET str = REPLACE(str, 'Õ', 'O');
    SET str = REPLACE(str, 'Ö', 'O');
    SET str = REPLACE(str, 'Ø', 'O');
    SET str = REPLACE(str, 'Ù', 'U');
    SET str = REPLACE(str, 'Ú', 'U');
    SET str = REPLACE(str, 'Û', 'U');
    SET str = REPLACE(str, 'Ü', 'U');
    SET str = REPLACE(str, 'Ý', 'Y');
    SET str = REPLACE(str, 'Þ', 'B');
    SET str = REPLACE(str, 'ß', 'Ss');
    SET str = REPLACE(str, 'à', 'a');
    SET str = REPLACE(str, 'á', 'a');
    SET str = REPLACE(str, 'â', 'a');
    SET str = REPLACE(str, 'ã', 'a');
    SET str = REPLACE(str, 'ä', 'a');
    SET str = REPLACE(str, 'å', 'a');
    SET str = REPLACE(str, 'æ', 'a');
    SET str = REPLACE(str, 'ç', 'c');
    SET str = REPLACE(str, 'è', 'e');
    SET str = REPLACE(str, 'é', 'e');
    SET str = REPLACE(str, 'ê', 'e');
    SET str = REPLACE(str, 'ë', 'e');
    SET str = REPLACE(str, 'ì', 'i');
    SET str = REPLACE(str, 'í', 'i');
    SET str = REPLACE(str, 'î', 'i');
    SET str = REPLACE(str, 'ï', 'i');
    SET str = REPLACE(str, 'ð', 'o');
    SET str = REPLACE(str, 'ñ', 'n');
    SET str = REPLACE(str, 'ò', 'o');
    SET str = REPLACE(str, 'ó', 'o');
    SET str = REPLACE(str, 'ô', 'o');
    SET str = REPLACE(str, 'õ', 'o');
    SET str = REPLACE(str, 'ö', 'o');
    SET str = REPLACE(str, 'ø', 'o');
    SET str = REPLACE(str, 'ù', 'u');
    SET str = REPLACE(str, 'ú', 'u');
    SET str = REPLACE(str, 'û', 'u');
    SET str = REPLACE(str, 'ý', 'y');
    SET str = REPLACE(str, 'ý', 'y');
    SET str = REPLACE(str, 'þ', 'b');
    SET str = REPLACE(str, 'ÿ', 'y');
    SET str = REPLACE(str, 'ƒ', 'f');


    RETURN str;
END;

#2
CREATE VIEW jugadores_karasuno_notildes AS
SELECT jugadores.id_jugador, QUITAR_TILDES(jugadores.nombre) as nombre
FROM jugadores
         JOIN jugadores_citados jc on jugadores.id_jugador = jc.id_jugador
         JOIN partidos p on p.id_partido = jc.id_partido
         JOIN equipos e on e.id_equipo = p.equipo_local
         JOIN equipos e2 on e2.id_equipo = p.equipo_visita
WHERE e.nombre like '%karasuno%'
   OR e2.nombre like '%karasuno%'
GROUP BY jugadores.id_jugador
ORDER BY id_jugador;

#3
-- auto-generated definition
create table historial_ligas
(
    id_historial_liga int auto_increment
        primary key,
    id_equipo         int                                  null,
    liga              tinytext                             null,
    fecha             datetime default current_timestamp() null,
    constraint historial_ligas_equipos_id_equipo_fk
        foreign key (id_equipo) references equipos (id_equipo)
)
    auto_increment = 3;

#4
CREATE TRIGGER historial_ligas_trigger_update
    AFTER UPDATE
    ON equipos
    FOR EACH ROW
    IF OLD.liga != NEW.liga THEN
        INSERT INTO historial_ligas
        SET id_equipo = OLD.id_equipo,
            liga=NEW.liga;
    END IF;

CREATE TRIGGER historial_ligas_trigger_insert
    AFTER INSERT
    ON equipos
    FOR EACH ROW
    INSERT INTO historial_ligas
    SET id_equipo = NEW.id_equipo,
        liga=NEW.liga;
        
        
        
########################################################################

#AP6
## Parte 1

#0.
SET autocommit = 0;

#1 Notar que se agrega el commit para que efectivamente quede guardado.
START TRANSACTION;
INSERT INTO haikyu.jugadores (nombre, fecha_nacimiento, estatura, peso, especialidad, debilidad, nacionalidad)
VALUES ('Hisashi Kinoshita', null, null, null, null, null, null);
COMMIT;

#2 Notar que estamos haciendo dos queries, y que estas no tienen efecto hasta que se haga el commit.
START TRANSACTION;

UPDATE jugadores
SET especialidad = 'ED'
WHERE LENGTH(nombre) > 15;

DELETE
FROM jugadores
WHERE nombre = 'Hisashi Kinoshita';

COMMIT;

#3 Notar que no borra nada, ya que se hace rollback.
START TRANSACTION;
DELETE FROM resultados_partidos;
ROLLBACK ;

## Parte 2
#1. Nada especial aqui
create schema AP6;

create table prueba
(
    id_prueba int auto_increment
        primary key,
    nombre    tinytext not null
);

#2. Creamos un usuario y hacemos que pueda hacer insert, select y update
# la tabla tiene que ser la misma de la parte 1.
create user usuario_ap6
    identified by 'local';
grant insert, select, update on ap6.prueba to usuario_ap6;

#3. Esto es hacer la conexion en datagrip o por consola
# deberian mostrar con un video lo que hicieron aqui

#4.
INSERT INTO ap6.prueba (nombre) VALUES ('holi');

#5.
UPDATE ap6.prueba t SET t.nombre = 'chai' WHERE t.id_prueba = 1;

#6.
DELETE FROM ap6.prueba WHERE id_prueba = 1;
# este es el error que deberian tener cuando intenten borrar.
# DELETE command denied to user 'usuario_ap6'@'localhost' for table 'prueba'




############################################################



#AP 7
#1
SELECT *
FROM (SELECT nombre,
             nacionalidad,
             TIMESTAMPDIFF(YEAR, fecha_nacimiento, NOW()) as edad,
             DENSE_RANK() over (ORDER BY edad DESC )      as dense
      FROM jugadores
      WHERE nacionalidad = 'japonesa'
      ORDER BY edad DESC) a
WHERE dense = 1;

#2
SELECT *
FROM (SELECT j.nombre, COUNT(*) as conteo, RANK() over (ORDER BY conteo DESC) as rank_conteo
      FROM jugadores_citados
               JOIN jugadores j on j.id_jugador = jugadores_citados.id_jugador
               JOIN partidos p on p.id_partido = jugadores_citados.id_partido
               JOIN equipos e on p.equipo_local = e.id_equipo
               JOIN equipos e2 on e2.id_equipo = p.equipo_visita
               JOIN estadios e3 on p.id_estadio = e3.id_estadio
      WHERE (e.nombre = 'Karasuno'
          OR e2.nombre = 'Karasuno')
        AND e3.direccion LIKE '%Tokyo%'
      GROUP BY j.id_jugador
      ORDER BY conteo DESC) a
WHERE a.rank_conteo = 1;

#3
SELECT IF(puntos_local > puntos_visita, e.nombre, e2.nombre)        as equipo,
       COUNT(IF(puntos_local > puntos_visita, e.nombre, e2.nombre)) as set_ganados,
       RANK() over (ORDER BY set_ganados DESC)                      as posicion
FROM resultados_partidos
         JOIN partidos p on p.id_partido = resultados_partidos.id_partido
         JOIN equipos e on e.id_equipo = p.equipo_local
         JOIN equipos e2 on e2.id_equipo = p.equipo_visita
GROUP BY IF(puntos_local > puntos_visita, e.nombre, e2.nombre);

#4
SELECT nombre, IFNULL(partidos_ganados, 0) as partidos_ganados, IFNULL(set_ganados, 0) as set_ganados,
       DENSE_RANK() over (ORDER BY partidos_ganados DESC , set_ganados DESC ) as posicion
FROM equipos
         LEFT JOIN
     (SELECT id_equipo, COUNT(*) as partidos_ganados
      FROM (SELECT e.nombre                                                                as nombre_local,
                   e2.nombre                                                               as nombre_visita,
                   p.id_partido,
                   IF(puntos_local > puntos_visita, e.id_equipo, e2.id_equipo)             as id_equipo,
                   DENSE_RANK() over (PARTITION BY p.id_partido ORDER BY set_ganados DESC) as ganador_partido,
                   COUNT(*)                                                                as set_ganados
            FROM resultados_partidos
                     JOIN partidos p on p.id_partido = resultados_partidos.id_partido
                     JOIN equipos e on e.id_equipo = p.equipo_local
                     JOIN equipos e2 on e2.id_equipo = p.equipo_visita
            GROUP BY p.id_partido, id_equipo) a
      WHERE ganador_partido = 1
      GROUP BY id_equipo) partidos ON equipos.id_equipo = partidos.id_equipo
         LEFT JOIN
     (SELECT id_equipo, SUM(set_ganados) as set_ganados
      FROM (SELECT p.id_partido,
                   IF(puntos_local > puntos_visita, e.id_equipo, e2.id_equipo) as id_equipo,
                   COUNT(*)                                                    as set_ganados
            FROM resultados_partidos
                     JOIN partidos p on p.id_partido = resultados_partidos.id_partido
                     JOIN equipos e on e.id_equipo = p.equipo_local
                     JOIN equipos e2 on e2.id_equipo = p.equipo_visita
            GROUP BY p.id_partido, id_equipo) a
      GROUP BY id_equipo) sets ON equipos.id_equipo = sets.id_equipo;
      
      
      
      
      
      
