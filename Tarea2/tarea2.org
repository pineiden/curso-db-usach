
#+TITLE: Proyecto semestral Bases de Datos

* Introducción

Este proyecto es grupal y consiste en modelar, implementar y crear las
consultas solicitadas.

Se debe realizar en postgres, mantener el proyecto bajo control de
versiones *git* y documentar cada decisión de diseño.

Este proyecto está basado en la novela *Los desposeídos* de /Ursula
K. Le Guin/, por lo que será necesario que leas previamente el libro
para realizarlo.

En el pequeño planeta(luna) de *Anarres* existe una sociedad organizada bajo
los principios de la anarquía, no hay jefes ni esclavos, es una
sociedad colaborativa y libre. De todas maneras están organizados de
manera democrática, cada pueblo tiene su propio enfoque productivo,
tienen redes de comunicación y comercio, centos de formación técnica y
universidades. Imagínatelo.

Pero este planeta lleva solo unos docientos años habitados. Un grupo
de rebeldes decidió migrar desde *Urras*, hartos del sistema de
opresión y abuso que se vivía. Generaron una revolución y tomaron unas
naves para viajar y terraformar lo que llamarían *Anarres*-

Esta tarea consiste en crear un sistema que administre todo el sistema
productivo de *Anarres*.

La plataforma debe ser capaz de realizar lo siguiente:

- Mantener un registro de eventos y sucesos históricos asociados a
  cada pueblo.
- Registrar cada pueblo y sus productos económicos
- Cada pueblo debe tener una ubicación geográfica
- Registrar los caminos, carreteras y líneas ferreas.
- Registrar los habitantes de Anarres
- Cada habitante tiene uno o más padres/madres, pasa por el sistema
  educativo libertario y se educa en algún oficio, profesión o
  ciencia.
- Cada sistema productivo necesitara una cierta cantidad de personas
  para administración, seguridad, transporte, comercio y la producción
  misma: sean alimentos, minería, vestimenta, etcétera.
- Hay eventos aleatorios como tormentas, sequías, terremotos que
  podrían afectar la frágil economía del planeta.
- Los eventos pueden gatillar la demanda de prioridades, por ejemplo
  la industria de alimentos por sobre la de vestimenta.

En cada pueblo hay un centro administrativo que gestiona las
asignaciones de trabajo, se encargará de:

- Registrar la actividad económica del pueblo
- Registro de eventos
- Ingreso y egresos de habitantes
- Solicitudes de cambio de trabajo
- Notificar a los habitantes de noticias y asignaciones
- Llevar registro de salarios
- Llevar registro de la producción y comercio
- Generar un reporte mensual

* Sugerencias.

- Para imaginarse el sistema leer el libro
- Crear/Inventar una historia
- La historia del pueblo de Anarres. 
- Puede ser un mapa con diversos lugares geográficos
- La historia podría ser en un periódo de 100 años
- Cada año suceden cosas/eventos
- Se pueden crear pueblos y caminos
- Cada camino conecta pueblos
- Simular la historia de cada pueblo 
- Lo que produce y demanda
- Simular eventos naturales que afecten la economía de cada pueblo
- Registrar demanda/oferta de trabajo
- Inventar curvas de población 
- Una vez se tenga la historia modelar el sistema de información

* Vistas y consultas

Se debe generar las siguientes consultas

- Cantidad de trabajadores por sector productivo 
- Montos de producción y ventas
- Trabajadores con su nivel de compatibilidad en el sector
  productivo. (Científicos construyendo caminos es menos compatible
  que obreros construyendo, pero podría ser)
- Nivel de desempleo por edad
- Cantidad de mujeres y hombres en edad fértil
- Cantidad de niños y niñas nacidas por periodo de tiempo.
- Emigrantes de otros pueblos, cantidad y detalle.
- Ancianos y jubilados, estado de salud 
- Tasa de natalidad por pueblo
- Nivel de estres de cada trabajador/habitante 
- Nivel de aburrimiento
- Seguimiento de comercio con otros pueblos
- Historia de cada pueblo
- Listado de responsables en cada pueblo en un período de tiempo
- Uso de caminos, generar un sistema de ruteo comercial simple con un
  algoritmo Dijkstra para la ruta más corta.

Proponer 10 consultas diferentes adicionales a las ya solicitadas.

Scripts a considerar:

- Creación de roles
- Creación de database
- Borrado de roles/database
- Cargado de datos
- Backup 



