Satellite data for your GIS
===========================

## About

Cloud-free satellite imagery mosaics based on Sentinel data.
Get easy-to-use satellite data using https://data.nextgis.com

## Contents

Each dataset may contain the following data layers:

	* Satellite imagery mosaic (file.tif)

## Source

European Space Agency (ESA)

## License

Original data:  
Contains modified Copernicus Sentinel data 2022-2023.  
Terms of use - https://sentinels.copernicus.eu/documents/247904/690755/Sentinel_Data_Legal_Notice  

Derivative data:  
© OpenDataBox (opendatabox.info)   
OpenDataBox database is made available under the Open Database License: https://www.opendatacommons.org/licenses/odbl  

QGIS project and layer styles:  
© NextGIS (data.nextgis.com)   
Licensed under CC-BY 4.0 - https://creativecommons.org/licenses/by/4.0/  

## Commercial support

Need to fix an error, add more fields or styles?  
Commercial support is available:   
 https://nextgis.com/contact
