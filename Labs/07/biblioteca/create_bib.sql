CREATE TABLE biblioteca.country (
	id SERIAL NOT NULL, 
	name VARCHAR(36), 
	continent VARCHAR(36) NOT NULL, 
	PRIMARY KEY (id), 
	UNIQUE (name)
)


2023-10-26 18:59:29,538 INFO sqlalchemy.engine.Engine [no key 0.00005s] {}
2023-10-26 18:59:29,542 INFO sqlalchemy.engine.Engine 
CREATE TABLE biblioteca.style (
	id SERIAL NOT NULL, 
	name VARCHAR(36), 
	description VARCHAR(256), 
	PRIMARY KEY (id), 
	UNIQUE (name)
)


2023-10-26 18:59:29,542 INFO sqlalchemy.engine.Engine [no key 0.00004s] {}
2023-10-26 18:59:29,544 INFO sqlalchemy.engine.Engine 
CREATE TABLE biblioteca.editorial (
	id SERIAL NOT NULL, 
	name VARCHAR(36), 
	web VARCHAR(512), 
	country_id INTEGER, 
	PRIMARY KEY (id), 
	UNIQUE (name), 
	FOREIGN KEY(country_id) REFERENCES biblioteca.country (id)
)


2023-10-26 18:59:29,544 INFO sqlalchemy.engine.Engine [no key 0.00005s] {}
2023-10-26 18:59:29,547 INFO sqlalchemy.engine.Engine 
CREATE TABLE biblioteca.author (
	id SERIAL NOT NULL, 
	name VARCHAR(36), 
	birth_year INTEGER, 
	country_id INTEGER, 
	PRIMARY KEY (id), 
	UNIQUE (name), 
	FOREIGN KEY(country_id) REFERENCES biblioteca.country (id)
)


2023-10-26 18:59:29,547 INFO sqlalchemy.engine.Engine [no key 0.00005s] {}
2023-10-26 18:59:29,550 INFO sqlalchemy.engine.Engine 
CREATE TABLE biblioteca.book (
	id SERIAL NOT NULL, 
	name VARCHAR(36), 
	pages INTEGER NOT NULL, 
	start_year INTEGER, 
	end_year INTEGER NOT NULL, 
	edicion_year INTEGER NOT NULL, 
	country_id INTEGER, 
	editorial_id INTEGER, 
	style_id INTEGER, 
	PRIMARY KEY (id), 
	CONSTRAINT book_check CHECK (start_year < end_year), 
	CONSTRAINT pages_check CHECK (0 < pages), 
	UNIQUE (name), 
	FOREIGN KEY(country_id) REFERENCES biblioteca.country (id), 
	FOREIGN KEY(editorial_id) REFERENCES biblioteca.editorial (id), 
	FOREIGN KEY(style_id) REFERENCES biblioteca.style (id)
)


2023-10-26 18:59:29,550 INFO sqlalchemy.engine.Engine [no key 0.00006s] {}
2023-10-26 18:59:29,553 INFO sqlalchemy.engine.Engine 
CREATE TABLE biblioteca.book_author (
	id SERIAL NOT NULL, 
	author_id INTEGER, 
	book_id INTEGER, 
	PRIMARY KEY (id), 
	FOREIGN KEY(author_id) REFERENCES biblioteca.author (id), 
	FOREIGN KEY(book_id) REFERENCES biblioteca.book (id)
)


2023-10-26 18:59:29,553 INFO sqlalchemy.engine.Engine [no key 0.00006s] {}
2023-10-26 18:59:29,554 INFO sqlalchemy.engine.Engine COMMIT
