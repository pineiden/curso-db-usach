from sqlalchemy import create_engine
from sqlalchemy.schema import CreateSchema
from sqlalchemy import event
from sqlalchemy.sql import exists, select
from biblioteca.models import Base
import os
from sqlalchemy import text


def create_biblioteca(engine):

    try:
        with engine.connect() as conn:
            if not engine.dialect.has_schema(conn, "biblioteca"):
                print("Crear schema...")
                create_schema = CreateSchema('biblioteca')
                print(create_schema)
                result = conn.execute(create_schema)
                conn.commit()
                print("Resultado", result)
                return True
            return False
        return False
    except Exception as e:
        raise e


if __name__ == '__main__':

    dbname = "biblioteca"
    dbuser = "david"
    dbpass = "oxido"
    dbhost = "localhost"
    dbport = 5432

    db_engine = 'postgresql://%s:%s@%s:%s/%s' % (dbuser, dbpass, dbhost,
                                                 dbport, dbname)
    engine = create_engine(db_engine, echo=True)
    print(db_engine)
    try:
        create_biblioteca(engine)
        Base.metadata.create_all(engine, checkfirst=True)
    except Exception as e:
        print("Falla al crear esquema de tablas")
        raise e
