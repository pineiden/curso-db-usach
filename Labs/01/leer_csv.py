import csv
from pathlib import Path
import sys
from rich import print
from clases import Meteo


def read_csv(archivo: Path):
    if archivo.exists():
        fields = ["name", "datetime", "preciptype",
                  "tempmax",
                  "tempmin",
                  "temp",
                  "feelslikemax",
                  "feelslikemin",
                  "feelslike",
                  "dew",
                  "humidity",
                  "precip",
                  "precipprob",
                  "precipcover",
                  "snow",
                  "snowdepth",
                  "windgust",
                  "windspeed",
                  "winddir",
                  "sealevelpressure",
                  "cloudcover",
                  "visibility",
                  "solarradiation",
                  "solarenergy",
                  "uvindex",
                  "severerisk",
                  "moonphase", "sunrise",
                  "sunset", "conditions", "description", "icon", "stations"]

        with archivo.open() as f:
            reader = csv.DictReader(f)
            for data in reader:
                meteo = Meteo(**{k: v for k, v in data.items() if k in
                                 fields})
                if meteo.preciptype:
                    print(meteo)
    else:
        print("El archivo no existe")


if __name__ == "__main__":
    print(sys.argv)
    archivo = Path(sys.argv[1])
    read_csv(archivo)
