from pydantic import BaseModel, Field, field_validator
from pydantic.dataclasses import dataclass
from datetime import datetime
from typing import List, Dict, Any
from enum import Enum
from pydantic import BaseModel, ValidationError
from pydantic.functional_validators import AfterValidator
from typing_extensions import Annotated
from pydantic import field_validator, validator


class PrecipType(Enum):
    NONE = 0
    RAIN = 1
    SNOW = 2

    @classmethod
    def fromstring(cls, valor: str):
        if valor.upper() == "RAIN":
            return PrecipType.RAIN
        elif valor.upper() == "SNOW":
            return PrecipType.SNOW
        else:
            return PrecipType.NONE

    @classmethod
    def from_list_string(cls, valores: str):
        lista = valores.split(",")
        resultados = []
        for valor in lista:
            valor = cls.fromstring(valor)
            resultados.append(valor)
        return resultados


class Icon(Enum):
    PartlyCloudyDay = 0
    ClearDay = 1
    Wind = 2
    Cloudy = 3
    Rain = 4
    Fog = 5
    NONE = 6

    @classmethod
    def fromstring(cls, valor: str):
        if valor.lower() == "partly-cloudy-day":
            return Icon.PartlyCloudyDay
        elif valor.lower() == "clear-day":
            return Icon.ClearDay
        elif valor.lower() == "wind":
            return Icon.Wind
        elif valor.lower() == "cloudy":
            return Icon.Cloudy
        elif valor.lower() == "rain":
            return Icon.Rain
        elif valor.lower() == "fog":
            return Icon.Fog
        else:
            return Icon.NONE


def stations_from_string(valores: str) -> List[str]:
    lista = valores.split(",")
    return lista


# @dataclass


class Meteo(BaseModel):
    name: str
    datetime: datetime
    preciptype: List[PrecipType]
    tempmax: float
    tempmin: float
    temp: float
    feelslikemax: float
    feelslikemin: float
    feelslike: float
    dew: float
    humidity: float
    precip: float
    precipprob: float
    precipcover: float
    snow: float
    snowdepth: float
    windgust: float
    windspeed: float
    winddir: float
    sealevelpressure: float
    cloudcover: float
    visibility: float
    solarradiation: float
    solarenergy: float
    uvindex: float
    severerisk: float
    moonphase: float
    sunrise: datetime
    sunset: datetime
    conditions: str
    description: str
    icon: str
    stations: List[str]

    @validator("datetime", pre=True)
    def datetime_validate(cls, value):
        try:
            formato = "%Y-%m-%d"
            fecha = datetime.strptime(value, formato)
            return fecha
        except ValueError as ve:
            raise ve

    @validator("preciptype", pre=True)
    def preciptype_valid(cls, v: str) -> List[PrecipType]:
        if v != "":
            return PrecipType.from_list_string(v)
        else:
            return []

    @validator("stations", pre=True)
    def stations_valid(cls, v: str) -> List[str]:
        return stations_from_string(v)

    @validator(
        "tempmax",
        "tempmin",
        "temp",
        "feelslikemax",
        "feelslikemin",
        "feelslike",
        "dew",
        "humidity",
        "precip",
        "precipprob",
        "precipcover",
        "snow",
        "snowdepth",
        "windgust",
        "windspeed",
        "winddir",
        "sealevelpressure",
        "cloudcover",
        "visibility",
        "solarradiation",
        "solarenergy",
        "uvindex",
        "severerisk",
        "moonphase", pre=True)
    def float_valid(cls, v: str):
        if v == "":
            return 0.0
        else:
            return float(v)

    # @classmethod
    # def fromdict(cls, data: Dict[str, Any]):
    #     formato = "%Y-%m-%d"
    #     newdata = {key: conversion(valor) for key, valor in data.items()}
    #     return cls(**newdata)
