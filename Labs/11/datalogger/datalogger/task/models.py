from django.db import models

# Create your models here.
class Task(models.Model):
    name = models.CharField(max_length=256)
    start = models.DateTimeField(auto_now=False)
    duration = models.IntegerField()
    application = models.CharField(max_length=256)

    class Meta:
        verbose_name_plural = "Tasks"

    def __repr__(self):
        return f"""Task(name={self.name}, start={self.start},
        duration={self.duration}, application={self.application})"""

    def __str__(self):
        return f"""Task(name={self.name}, start={self.start},
        duration={self.duration}, application={self.application})"""
