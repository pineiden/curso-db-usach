import json
from sqlalchemy.orm import relationship
from sqlalchemy.orm import validates
from sqlalchemy import UniqueConstraint, Index
from sqlalchemy import Column, Integer, Text, String, DateTime, ForeignKey
from sqlalchemy import Boolean
from sqlalchemy.types import NUMERIC

from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy.ext.declarative import declared_attr
from sqlalchemy.ext.hybrid import hybrid_property

from sqlalchemy.orm import Mapped
from typing import List

# add constraints
from sqlalchemy import CheckConstraint
from sqlalchemy.orm import mapped_column
from sqlalchemy.orm import declared_attr

Base = declarative_base()


class Country(Base):
    __tablename__ = 'country'
    __table_args__ = {'schema': 'biblioteca'}

    id = Column(Integer, primary_key=True, autoincrement=True)
    name = Column(String(36), unique=True)
    continent = Column(String(36), nullable=False)
    flag = Column(String(512), nullable=False)
    main_city = Column(String(64), nullable=False)

    editorials: Mapped[List["Editorial"]] = relationship(
        back_populates='country')
    authors: Mapped[List["Author"]] = relationship(back_populates='country')
    books: Mapped[List["Book"]] = relationship(back_populates='country')

    def __repr__(self):
        return f"Country({self.id},{self.name},{self.continent})"

    def __str__(self):
        return f"Country({self.id},{self.name},{self.continent})"


class Style(Base):
    __tablename__ = 'style'
    __table_args__ = {'schema': 'biblioteca'}

    id = Column(Integer, primary_key=True, autoincrement=True)
    name = Column(String(36), unique=True)
    description = Column(String(1024))

    books: Mapped[List["Book"]] = relationship(back_populates='style')

    def __repr__(self):
        return f"Style({self.id},{self.name})"

    def __str__(self):
        return f"Style({self.id},{self.name})"


class Editorial(Base):
    __tablename__ = 'editorial'
    __table_args__ = {'schema': 'biblioteca'}

    id = Column(Integer, primary_key=True, autoincrement=True)
    name = Column(String(36), unique=True)
    web = Column(String(512))
    country_id = Column(Integer, ForeignKey('biblioteca.country.id'))

    country: Mapped["Country"] = relationship(back_populates='editorials')
    books: Mapped[List["Book"]] = relationship(back_populates='editorial')

    def __repr__(self):
        return f"Editorial({self.id},{self.name})"

    def __str__(self):
        return f"Editorial({self.id},{self.name})"


class Author(Base):
    __tablename__ = 'author'
    __table_args__ = {'schema': 'biblioteca'}

    id = Column(Integer, primary_key=True, autoincrement=True)
    name = Column(String(36), unique=True)
    birth_year = Column(Integer)

    country_id = Column(Integer, ForeignKey('biblioteca.country.id'))
    country: Mapped["Country"] = relationship(back_populates='authors')

    book_authors: Mapped[
        List["BookAuthor"]] = relationship(back_populates='author')

    def __repr__(self):
        return f"Author({self.id},{self.name}, {self.year})"

    def __str__(self):
        return f"Author({self.id},{self.name})"


class AbstractBook(Base):
    __abstract__ = True
    
    @declared_attr.directive
    def __table_args__(cls):
        return (
            CheckConstraint("start_year < end_year",
                            name="book_check"),
            CheckConstraint("0 < pages",
                            name="pages_check"),
            {'schema': 'biblioteca'}, # last argument dict
        )
    
    id: Mapped[int] = mapped_column(primary_key=True)
    pages:Mapped[int]
    start_year:Mapped[int]
    end_year:Mapped[int]
    

class Book(AbstractBook):
    __tablename__ = 'book'
    #__table_args__ = {'schema': 'biblioteca'}

    id = Column(Integer, primary_key=True, autoincrement=True)
    name = Column(String(256), unique=True)
    pages = Column(Integer, nullable=False)
    start_year = Column(Integer, nullable=True)
    end_year = Column(Integer, nullable=True)
    edition = Column(Integer, nullable=False)

    country_id = Column(Integer, ForeignKey('biblioteca.country.id'))
    country: Mapped["Country"] = relationship(back_populates='books')

    editorial_id = Column(Integer, ForeignKey('biblioteca.editorial.id'))
    editorial: Mapped["Editorial"] = relationship(back_populates='books')

    style_id = Column(Integer, ForeignKey('biblioteca.style.id'))
    style: Mapped["Style"] = relationship(back_populates='books')

    book_authors: Mapped[List["BookAuthor"]] = relationship(
        'BookAuthor', back_populates='book')

    def __repr__(self):
        return f"Book({self.id},{self.name}, {self.edicion_year})"

    def __str__(self):
        return f"Book({self.id},{self.name})"


class BookAuthor(Base):
    __tablename__ = 'book_author'
    __table_args__ = {'schema': 'biblioteca'}

    id = Column(Integer, primary_key=True, autoincrement=True)

    author_id = Column(Integer, ForeignKey('biblioteca.author.id'))
    author: Mapped["Author"] = relationship(back_populates='book_authors')

    book_id = Column(Integer, ForeignKey('biblioteca.book.id'))
    book: Mapped["Book"] = relationship(back_populates='book_authors')

    def __repr__(self):
        return f"BookAuthor({self.id},{self.author_id}, {self.book_id})"

    def __str__(self):
        return f"BookAuthor({self.id},{self.author_id}, {self.book_id})"
