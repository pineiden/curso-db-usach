--
-- PostgreSQL database dump
--

-- Dumped from database version 14.9 (Ubuntu 14.9-0ubuntu0.22.04.1)
-- Dumped by pg_dump version 14.9 (Ubuntu 14.9-0ubuntu0.22.04.1)

SET statement_timeout = 0;
SET lock_timeout = 0;
SET idle_in_transaction_session_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SELECT pg_catalog.set_config('search_path', '', false);
SET check_function_bodies = false;
SET xmloption = content;
SET client_min_messages = warning;
SET row_security = off;

--
-- Name: biblioteca; Type: SCHEMA; Schema: -; Owner: david
--

CREATE SCHEMA biblioteca;


ALTER SCHEMA biblioteca OWNER TO david;

SET default_tablespace = '';

SET default_table_access_method = heap;

--
-- Name: author; Type: TABLE; Schema: biblioteca; Owner: david
--

CREATE TABLE biblioteca.author (
    id integer NOT NULL,
    name character varying(36),
    birth_year integer,
    country_id integer
);


ALTER TABLE biblioteca.author OWNER TO david;

--
-- Name: author_id_seq; Type: SEQUENCE; Schema: biblioteca; Owner: david
--

CREATE SEQUENCE biblioteca.author_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE biblioteca.author_id_seq OWNER TO david;

--
-- Name: author_id_seq; Type: SEQUENCE OWNED BY; Schema: biblioteca; Owner: david
--

ALTER SEQUENCE biblioteca.author_id_seq OWNED BY biblioteca.author.id;


--
-- Name: book; Type: TABLE; Schema: biblioteca; Owner: david
--

CREATE TABLE biblioteca.book (
    id integer NOT NULL,
    name character varying(256),
    pages integer NOT NULL,
    start_year integer,
    end_year integer,
    edition integer NOT NULL,
    country_id integer,
    editorial_id integer,
    style_id integer,
    CONSTRAINT book_check CHECK ((start_year < end_year)),
    CONSTRAINT pages_check CHECK ((0 < pages))
);


ALTER TABLE biblioteca.book OWNER TO david;

--
-- Name: book_author; Type: TABLE; Schema: biblioteca; Owner: david
--

CREATE TABLE biblioteca.book_author (
    id integer NOT NULL,
    author_id integer,
    book_id integer
);


ALTER TABLE biblioteca.book_author OWNER TO david;

--
-- Name: book_author_id_seq; Type: SEQUENCE; Schema: biblioteca; Owner: david
--

CREATE SEQUENCE biblioteca.book_author_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE biblioteca.book_author_id_seq OWNER TO david;

--
-- Name: book_author_id_seq; Type: SEQUENCE OWNED BY; Schema: biblioteca; Owner: david
--

ALTER SEQUENCE biblioteca.book_author_id_seq OWNED BY biblioteca.book_author.id;


--
-- Name: book_id_seq; Type: SEQUENCE; Schema: biblioteca; Owner: david
--

CREATE SEQUENCE biblioteca.book_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE biblioteca.book_id_seq OWNER TO david;

--
-- Name: book_id_seq; Type: SEQUENCE OWNED BY; Schema: biblioteca; Owner: david
--

ALTER SEQUENCE biblioteca.book_id_seq OWNED BY biblioteca.book.id;


--
-- Name: country; Type: TABLE; Schema: biblioteca; Owner: david
--

CREATE TABLE biblioteca.country (
    id integer NOT NULL,
    name character varying(36),
    continent character varying(36) NOT NULL,
    flag character varying(512) NOT NULL,
    main_city character varying(64) NOT NULL
);


ALTER TABLE biblioteca.country OWNER TO david;

--
-- Name: country_id_seq; Type: SEQUENCE; Schema: biblioteca; Owner: david
--

CREATE SEQUENCE biblioteca.country_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE biblioteca.country_id_seq OWNER TO david;

--
-- Name: country_id_seq; Type: SEQUENCE OWNED BY; Schema: biblioteca; Owner: david
--

ALTER SEQUENCE biblioteca.country_id_seq OWNED BY biblioteca.country.id;


--
-- Name: editorial; Type: TABLE; Schema: biblioteca; Owner: david
--

CREATE TABLE biblioteca.editorial (
    id integer NOT NULL,
    name character varying(36),
    web character varying(512),
    country_id integer
);


ALTER TABLE biblioteca.editorial OWNER TO david;

--
-- Name: editorial_id_seq; Type: SEQUENCE; Schema: biblioteca; Owner: david
--

CREATE SEQUENCE biblioteca.editorial_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE biblioteca.editorial_id_seq OWNER TO david;

--
-- Name: editorial_id_seq; Type: SEQUENCE OWNED BY; Schema: biblioteca; Owner: david
--

ALTER SEQUENCE biblioteca.editorial_id_seq OWNED BY biblioteca.editorial.id;


--
-- Name: style; Type: TABLE; Schema: biblioteca; Owner: david
--

CREATE TABLE biblioteca.style (
    id integer NOT NULL,
    name character varying(36),
    description character varying(1024)
);


ALTER TABLE biblioteca.style OWNER TO david;

--
-- Name: style_id_seq; Type: SEQUENCE; Schema: biblioteca; Owner: david
--

CREATE SEQUENCE biblioteca.style_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE biblioteca.style_id_seq OWNER TO david;

--
-- Name: style_id_seq; Type: SEQUENCE OWNED BY; Schema: biblioteca; Owner: david
--

ALTER SEQUENCE biblioteca.style_id_seq OWNED BY biblioteca.style.id;


--
-- Name: author id; Type: DEFAULT; Schema: biblioteca; Owner: david
--

ALTER TABLE ONLY biblioteca.author ALTER COLUMN id SET DEFAULT nextval('biblioteca.author_id_seq'::regclass);


--
-- Name: book id; Type: DEFAULT; Schema: biblioteca; Owner: david
--

ALTER TABLE ONLY biblioteca.book ALTER COLUMN id SET DEFAULT nextval('biblioteca.book_id_seq'::regclass);


--
-- Name: book_author id; Type: DEFAULT; Schema: biblioteca; Owner: david
--

ALTER TABLE ONLY biblioteca.book_author ALTER COLUMN id SET DEFAULT nextval('biblioteca.book_author_id_seq'::regclass);


--
-- Name: country id; Type: DEFAULT; Schema: biblioteca; Owner: david
--

ALTER TABLE ONLY biblioteca.country ALTER COLUMN id SET DEFAULT nextval('biblioteca.country_id_seq'::regclass);


--
-- Name: editorial id; Type: DEFAULT; Schema: biblioteca; Owner: david
--

ALTER TABLE ONLY biblioteca.editorial ALTER COLUMN id SET DEFAULT nextval('biblioteca.editorial_id_seq'::regclass);


--
-- Name: style id; Type: DEFAULT; Schema: biblioteca; Owner: david
--

ALTER TABLE ONLY biblioteca.style ALTER COLUMN id SET DEFAULT nextval('biblioteca.style_id_seq'::regclass);


--
-- Data for Name: author; Type: TABLE DATA; Schema: biblioteca; Owner: david
--

COPY biblioteca.author (id, name, birth_year, country_id) FROM stdin;
1	gabriel garcia marquez	1927	\N
2	jk rowling	1965	\N
3	jrr tolkien	1892	\N
4	julio verne	1828	\N
5	julio cortazar	1914	\N
6	alicia morel	1921	\N
7	marcela paz	1902	\N
8	manuel rojas	1896	\N
9	liu cixin	1963	\N
10	baldomero lillo	1867	\N
11	pablo neruda	1904	\N
12	nicanor parra	1914	\N
13	anonimo	\N	\N
14	leon tolstoi	1828	\N
15	alejandro dumas	1802	\N
\.


--
-- Data for Name: book; Type: TABLE DATA; Schema: biblioteca; Owner: david
--

COPY biblioteca.book (id, name, pages, start_year, end_year, edition, country_id, editorial_id, style_id) FROM stdin;
1	cien años de soledad	471	\N	1982	2003	1	1	2
2	harry potter	3792	\N	1997	2020	2	2	2
3	el señor de los anillos, la comunidad del anillo	1392	\N	1954	2016	2	3	2
4	miguel strogoff	382	\N	1876	2018	3	4	2
5	rayuela	632	\N	1963	2013	5	5	2
6	perico trepa por chile	182	\N	1978	2000	4	6	2
7	hijo de ladron	312	\N	1951	2013	4	7	2
8	el problema de los tres cuerpos I	1728	\N	2006	2021	8	8	2
9	el coronel no tiene quien le escriba	104	\N	1961	2003	1	9	2
10	la vuelta al mundo en 80 dias	304	\N	1872	2002	3	10	2
11	la vuelta al dia en 80 mundos	214	\N	1967	2010	5	11	2
12	sub terra	84	\N	1904	2014	4	12	2
13	sub sole	94	\N	1907	2018	4	12	2
14	20 poemas de amor y una cancion desesperada	112	\N	1924	2009	4	13	1
15	canto general	468	\N	1950	2018	4	14	1
16	poemas y antipoemas	112	\N	1954	2001	4	15	1
17	mil y una noches	2128	\N	\N	2021	6	16	2
18	guerra y paz	1384	\N	1965	2015	7	17	2
19	el conde de montecristo	1448	1884	1885	2016	3	18	2
\.


--
-- Data for Name: book_author; Type: TABLE DATA; Schema: biblioteca; Owner: david
--

COPY biblioteca.book_author (id, author_id, book_id) FROM stdin;
1	1	1
2	2	2
3	3	3
4	4	4
5	5	5
6	6	6
7	7	6
8	8	7
9	9	8
10	1	9
11	4	10
12	5	11
13	10	12
14	10	13
15	11	14
16	11	15
17	12	16
18	13	17
19	14	18
20	15	19
\.


--
-- Data for Name: country; Type: TABLE DATA; Schema: biblioteca; Owner: david
--

COPY biblioteca.country (id, name, continent, flag, main_city) FROM stdin;
1	colombia	América	https://commons.wikimedia.org/wiki/File:Flag_of_Colombia.svg#/media/Archivo:Flag_of_Colombia.svg	Bogotá
2	reino unido	Europa	https://commons.wikimedia.org/wiki/File:Flag_of_the_United_Kingdom_(3-5).svg#/media/Archivo:Flag_of_the_United_Kingdom_(3-5).svg	Londres
3	francia	Europa	https://commons.wikimedia.org/wiki/File:Flag_of_France_(1794%E2%80%931815,_1830%E2%80%931974).svg#/media/Archivo:Flag_of_France_(1794–1815,_1830–1974).svg	París
4	chile	América	https://commons.wikimedia.org/wiki/File:Flag_of_Chile.svg#/media/Archivo:Flag_of_Chile.svg	Santiago
5	argentina	América	https://commons.wikimedia.org/wiki/File:Flag_of_Argentina.svg#/media/Archivo:Flag_of_Argentina.svg	Buenos Aires
6	arabia	Asia	https://commons.wikimedia.org/wiki/File:Arabian_Peninsula_dust_SeaWiFS.jpg#/media/Archivo:Arabian_Peninsula_dust_SeaWiFS.jpg	Riad
7	rusia	Europa, Asia	https://commons.wikimedia.org/wiki/File:Flag_of_Russia.svg#/media/Archivo:Flag_of_Russia.svg	Moscú
8	china	Asia	https://commons.wikimedia.org/wiki/File:Flag_of_the_People%27s_Republic_of_China.svg#/media/Archivo:Flag_of_the_People's_Republic_of_China.svg	Pekín
9	españa	Europa	https://commons.wikimedia.org/wiki/File:Bandera_de_Espa%C3%B1a.svg#/media/Archivo:Bandera_de_España.svg	Madrid
\.


--
-- Data for Name: editorial; Type: TABLE DATA; Schema: biblioteca; Owner: david
--

COPY biblioteca.editorial (id, name, web, country_id) FROM stdin;
1	contemporanea	https://contemporanea-ediciones.com.ar/	5
2	salamandra	https://www.penguinlibros.com/es/11942-salamandra	9
3	booket	https://www.planeta.es/es/booket	9
4	mestas ediciones	https://mestasediciones.com/	9
5	alfaguara	https://www.penguinlibros.com/es/11579-alfaguara	9
6	sm	https://www.tiendasm.cl/produccion/	4
7	zigzag	https://www.zigzag.cl/	4
8	nova	https://www.penguinlibros.com/es/11773-nova	9
9	debolsillo	https://www.penguinlibros.com/es/11338-debolsillo	9
10	anaya	https://www.grupoanaya.es/	9
11	rm	https://editorialrm.com/	9
12	edición digital	https://ediciondigital.cl/	4
13	edaf	https://www.edaf.net/	9
14	seix barral	https://www.planetadelibros.com/editorial/seix-barral/9	9
15	editorial universitaria	https://www.universitaria.cl/	4
16	longseller	https://longseller.com.ar/	5
17	alianza editorial	https://www.alianzaeditorial.es/	9
18	akal	https://www.akal.com/	9
\.


--
-- Data for Name: style; Type: TABLE DATA; Schema: biblioteca; Owner: david
--

COPY biblioteca.style (id, name, description) FROM stdin;
1	poesia	La poesía es un género literario que se caracteriza por el uso de la expresión artística a través del lenguaje, la métrica y la rima. Los poetas utilizan la musicalidad y la metáfora para transmitir emociones, imágenes y significados profundos en un formato compacto y lírico.
2	narrativa	La narrativa es un género literario que se centra en la creación de historias y relatos a través de la palabra escrita. Incluye novelas, cuentos, epopeyas y cualquier forma de escritura que tenga una estructura narrativa. Los narradores pueden ser en tercera persona u omniscientes, primera persona o incluso múltiples voces narrativas.
3	filosofía	En el contexto de géneros literarios, la filosofía se refiere a la escritura filosófica que busca explorar cuestiones fundamentales sobre la existencia, la moral, el conocimiento y la realidad. Los textos filosóficos son discursivos y argumentativos, y los filósofos utilizan la escritura para analizar, debatir y proponer teorías sobre temas abstractos y conceptuales. Los géneros filosóficos incluyen ensayos filosóficos, diálogos socráticos y tratados filosóficos.
\.


--
-- Name: author_id_seq; Type: SEQUENCE SET; Schema: biblioteca; Owner: david
--

SELECT pg_catalog.setval('biblioteca.author_id_seq', 15, true);


--
-- Name: book_author_id_seq; Type: SEQUENCE SET; Schema: biblioteca; Owner: david
--

SELECT pg_catalog.setval('biblioteca.book_author_id_seq', 20, true);


--
-- Name: book_id_seq; Type: SEQUENCE SET; Schema: biblioteca; Owner: david
--

SELECT pg_catalog.setval('biblioteca.book_id_seq', 19, true);


--
-- Name: country_id_seq; Type: SEQUENCE SET; Schema: biblioteca; Owner: david
--

SELECT pg_catalog.setval('biblioteca.country_id_seq', 9, true);


--
-- Name: editorial_id_seq; Type: SEQUENCE SET; Schema: biblioteca; Owner: david
--

SELECT pg_catalog.setval('biblioteca.editorial_id_seq', 18, true);


--
-- Name: style_id_seq; Type: SEQUENCE SET; Schema: biblioteca; Owner: david
--

SELECT pg_catalog.setval('biblioteca.style_id_seq', 3, true);


--
-- Name: author author_name_key; Type: CONSTRAINT; Schema: biblioteca; Owner: david
--

ALTER TABLE ONLY biblioteca.author
    ADD CONSTRAINT author_name_key UNIQUE (name);


--
-- Name: author author_pkey; Type: CONSTRAINT; Schema: biblioteca; Owner: david
--

ALTER TABLE ONLY biblioteca.author
    ADD CONSTRAINT author_pkey PRIMARY KEY (id);


--
-- Name: book_author book_author_pkey; Type: CONSTRAINT; Schema: biblioteca; Owner: david
--

ALTER TABLE ONLY biblioteca.book_author
    ADD CONSTRAINT book_author_pkey PRIMARY KEY (id);


--
-- Name: book book_name_key; Type: CONSTRAINT; Schema: biblioteca; Owner: david
--

ALTER TABLE ONLY biblioteca.book
    ADD CONSTRAINT book_name_key UNIQUE (name);


--
-- Name: book book_pkey; Type: CONSTRAINT; Schema: biblioteca; Owner: david
--

ALTER TABLE ONLY biblioteca.book
    ADD CONSTRAINT book_pkey PRIMARY KEY (id);


--
-- Name: country country_name_key; Type: CONSTRAINT; Schema: biblioteca; Owner: david
--

ALTER TABLE ONLY biblioteca.country
    ADD CONSTRAINT country_name_key UNIQUE (name);


--
-- Name: country country_pkey; Type: CONSTRAINT; Schema: biblioteca; Owner: david
--

ALTER TABLE ONLY biblioteca.country
    ADD CONSTRAINT country_pkey PRIMARY KEY (id);


--
-- Name: editorial editorial_name_key; Type: CONSTRAINT; Schema: biblioteca; Owner: david
--

ALTER TABLE ONLY biblioteca.editorial
    ADD CONSTRAINT editorial_name_key UNIQUE (name);


--
-- Name: editorial editorial_pkey; Type: CONSTRAINT; Schema: biblioteca; Owner: david
--

ALTER TABLE ONLY biblioteca.editorial
    ADD CONSTRAINT editorial_pkey PRIMARY KEY (id);


--
-- Name: style style_name_key; Type: CONSTRAINT; Schema: biblioteca; Owner: david
--

ALTER TABLE ONLY biblioteca.style
    ADD CONSTRAINT style_name_key UNIQUE (name);


--
-- Name: style style_pkey; Type: CONSTRAINT; Schema: biblioteca; Owner: david
--

ALTER TABLE ONLY biblioteca.style
    ADD CONSTRAINT style_pkey PRIMARY KEY (id);


--
-- Name: author author_country_id_fkey; Type: FK CONSTRAINT; Schema: biblioteca; Owner: david
--

ALTER TABLE ONLY biblioteca.author
    ADD CONSTRAINT author_country_id_fkey FOREIGN KEY (country_id) REFERENCES biblioteca.country(id);


--
-- Name: book_author book_author_author_id_fkey; Type: FK CONSTRAINT; Schema: biblioteca; Owner: david
--

ALTER TABLE ONLY biblioteca.book_author
    ADD CONSTRAINT book_author_author_id_fkey FOREIGN KEY (author_id) REFERENCES biblioteca.author(id);


--
-- Name: book_author book_author_book_id_fkey; Type: FK CONSTRAINT; Schema: biblioteca; Owner: david
--

ALTER TABLE ONLY biblioteca.book_author
    ADD CONSTRAINT book_author_book_id_fkey FOREIGN KEY (book_id) REFERENCES biblioteca.book(id);


--
-- Name: book book_country_id_fkey; Type: FK CONSTRAINT; Schema: biblioteca; Owner: david
--

ALTER TABLE ONLY biblioteca.book
    ADD CONSTRAINT book_country_id_fkey FOREIGN KEY (country_id) REFERENCES biblioteca.country(id);


--
-- Name: book book_editorial_id_fkey; Type: FK CONSTRAINT; Schema: biblioteca; Owner: david
--

ALTER TABLE ONLY biblioteca.book
    ADD CONSTRAINT book_editorial_id_fkey FOREIGN KEY (editorial_id) REFERENCES biblioteca.editorial(id);


--
-- Name: book book_style_id_fkey; Type: FK CONSTRAINT; Schema: biblioteca; Owner: david
--

ALTER TABLE ONLY biblioteca.book
    ADD CONSTRAINT book_style_id_fkey FOREIGN KEY (style_id) REFERENCES biblioteca.style(id);


--
-- Name: editorial editorial_country_id_fkey; Type: FK CONSTRAINT; Schema: biblioteca; Owner: david
--

ALTER TABLE ONLY biblioteca.editorial
    ADD CONSTRAINT editorial_country_id_fkey FOREIGN KEY (country_id) REFERENCES biblioteca.country(id);


--
-- PostgreSQL database dump complete
--

