--
-- PostgreSQL database cluster dump
--

SET default_transaction_read_only = off;

SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;



--
-- Role memberships
--



--
-- Databases
--

--
-- Database "template1" dump
--

\connect template1

--
-- PostgreSQL database dump
--

-- Dumped from database version 14.9 (Ubuntu 14.9-0ubuntu0.22.04.1)
-- Dumped by pg_dump version 14.9 (Ubuntu 14.9-0ubuntu0.22.04.1)

SET statement_timeout = 0;
SET lock_timeout = 0;
SET idle_in_transaction_session_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SELECT pg_catalog.set_config('search_path', '', false);
SET check_function_bodies = false;
SET xmloption = content;
SET client_min_messages = warning;
SET row_security = off;

--
-- PostgreSQL database dump complete
--

--
-- Database "biblioteca" dump
--

--
-- PostgreSQL database dump
--

-- Dumped from database version 14.9 (Ubuntu 14.9-0ubuntu0.22.04.1)
-- Dumped by pg_dump version 14.9 (Ubuntu 14.9-0ubuntu0.22.04.1)

SET statement_timeout = 0;
SET lock_timeout = 0;
SET idle_in_transaction_session_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SELECT pg_catalog.set_config('search_path', '', false);
SET check_function_bodies = false;
SET xmloption = content;
SET client_min_messages = warning;
SET row_security = off;

--
-- Name: biblioteca; Type: DATABASE; Schema: -; Owner: martin
--

CREATE DATABASE biblioteca WITH TEMPLATE = template0 ENCODING = 'UTF8' LOCALE = 'es_CL.UTF-8';


ALTER DATABASE biblioteca OWNER TO david;

\connect biblioteca

SET statement_timeout = 0;
SET lock_timeout = 0;
SET idle_in_transaction_session_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SELECT pg_catalog.set_config('search_path', '', false);
SET check_function_bodies = false;
SET xmloption = content;
SET client_min_messages = warning;
SET row_security = off;

SET default_tablespace = '';

SET default_table_access_method = heap;

--
-- Name: author; Type: TABLE; Schema: public; Owner: martin
--

CREATE TABLE public.author (
    id integer NOT NULL,
    name character varying(50),
    birth_year integer
);


ALTER TABLE public.author OWNER TO martin;

--
-- Name: author_id_seq; Type: SEQUENCE; Schema: public; Owner: david
--

CREATE SEQUENCE biblioteca.author_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE biblioteca.author_id_seq OWNER TO david;

--
-- Name: author_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: david
--

ALTER SEQUENCE biblioteca.author_id_seq OWNED BY biblioteca.author.id;


--
-- Name: book; Type: TABLE; Schema: public; Owner: david
--

CREATE TABLE biblioteca.book (
    id integer NOT NULL,
    name character varying(255),
    pages integer NOT NULL,
    start_year integer,
    end_year integer,
    country_id integer,
    editorial_id integer,
    edition integer,
    style_id integer,
    CONSTRAINT book_check CHECK ((end_year >= start_year)),
    CONSTRAINT pages_check CHECK ((pages > 0))
);


ALTER TABLE biblioteca.book OWNER TO david;

--
-- Name: book_author; Type: TABLE; Schema: public; Owner: david
--

CREATE TABLE biblioteca.book_author (
    id integer NOT NULL,
    book_id integer,
    author_id integer
);


ALTER TABLE biblioteca.book_author OWNER TO david;

--
-- Name: book_author_id_seq; Type: SEQUENCE; Schema: public; Owner: david
--

CREATE SEQUENCE biblioteca.book_author_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE biblioteca.book_author_id_seq OWNER TO david;

--
-- Name: book_author_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: david
--

ALTER SEQUENCE biblioteca.book_author_id_seq OWNED BY biblioteca.book_author.id;


--
-- Name: books_id_seq; Type: SEQUENCE; Schema: public; Owner: david
--

CREATE SEQUENCE biblioteca.books_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE biblioteca.books_id_seq OWNER TO david;

--
-- Name: books_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: david
--

ALTER SEQUENCE biblioteca.books_id_seq OWNED BY biblioteca.book.id;


--
-- Name: country; Type: TABLE; Schema: public; Owner: david
--

CREATE TABLE biblioteca.country (
    id integer NOT NULL,
    name character varying(50),
    flag text,
    continent character varying(50),
    main_city character varying(50)
);


ALTER TABLE biblioteca.country OWNER TO david;

--
-- Name: country_id_seq; Type: SEQUENCE; Schema: public; Owner: david
--

CREATE SEQUENCE biblioteca.country_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE biblioteca.country_id_seq OWNER TO david;

--
-- Name: country_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: david
--

ALTER SEQUENCE biblioteca.country_id_seq OWNED BY biblioteca.country.id;


--
-- Name: editorial; Type: TABLE; Schema: public; Owner: david
--

CREATE TABLE biblioteca.editorial (
    id integer NOT NULL,
    name character varying(50),
    web character varying(255),
    country_id integer
);


ALTER TABLE biblioteca.editorial OWNER TO david;

--
-- Name: editorial_id_seq; Type: SEQUENCE; Schema: public; Owner: david
--

CREATE SEQUENCE biblioteca.editorial_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE biblioteca.editorial_id_seq OWNER TO david;

--
-- Name: editorial_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: david
--

ALTER SEQUENCE biblioteca.editorial_id_seq OWNED BY biblioteca.editorial.id;


--
-- Name: style; Type: TABLE; Schema: public; Owner: david
--

CREATE TABLE biblioteca.style (
    id integer NOT NULL,
    name character varying(50),
    description text
);


ALTER TABLE biblioteca.style OWNER TO david;

--
-- Name: style_id_seq; Type: SEQUENCE; Schema: public; Owner: david
--

CREATE SEQUENCE biblioteca.style_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE biblioteca.style_id_seq OWNER TO david;

--
-- Name: style_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: david
--

ALTER SEQUENCE biblioteca.style_id_seq OWNED BY biblioteca.style.id;


--
-- Name: author id; Type: DEFAULT; Schema: public; Owner: david
--

ALTER TABLE ONLY biblioteca.author ALTER COLUMN id SET DEFAULT nextval('biblioteca.author_id_seq'::regclass);


--
-- Name: book id; Type: DEFAULT; Schema: public; Owner: david
--

ALTER TABLE ONLY biblioteca.book ALTER COLUMN id SET DEFAULT nextval('biblioteca.books_id_seq'::regclass);


--
-- Name: book_author id; Type: DEFAULT; Schema: public; Owner: david
--

ALTER TABLE ONLY biblioteca.book_author ALTER COLUMN id SET DEFAULT nextval('biblioteca.book_author_id_seq'::regclass);


--
-- Name: country id; Type: DEFAULT; Schema: public; Owner: david
--

ALTER TABLE ONLY biblioteca.country ALTER COLUMN id SET DEFAULT nextval('biblioteca.country_id_seq'::regclass);


--
-- Name: editorial id; Type: DEFAULT; Schema: public; Owner: david
--

ALTER TABLE ONLY biblioteca.editorial ALTER COLUMN id SET DEFAULT nextval('biblioteca.editorial_id_seq'::regclass);


--
-- Name: style id; Type: DEFAULT; Schema: public; Owner: david
--

ALTER TABLE ONLY biblioteca.style ALTER COLUMN id SET DEFAULT nextval('biblioteca.style_id_seq'::regclass);


--
-- Data for Name: author; Type: TABLE DATA; Schema: public; Owner: david
--

COPY biblioteca.author (id, name, birth_year) FROM stdin;
1	gabriel garcia marquez	1927
2	jk rowling	1965
3	jrr tolkien	1892
4	julio verne	1828
5	julio cortazar	1914
6	alicia morel	1921
7	marcela paz	1902
8	manuel rojas	1896
9	liu cixin	1963
10	baldomero lillo	1867
11	pablo neruda	1904
12	nicanor parra	1914
13	anonimo	\N
14	leon tolstoi	1828
15	alejandro dumas	1802
\.


--
-- Data for Name: book; Type: TABLE DATA; Schema: public; Owner: david
--

COPY biblioteca.book (id, name, pages, start_year, end_year, country_id, editorial_id, edition, style_id) FROM stdin;
1	cien años de soledad	471	\N	1982	1	1	2003	2
2	harry potter	3792	\N	1997	2	2	2020	2
3	el señor de los anillos, la comunidad del anillo	1392	\N	1954	2	3	2016	2
4	miguel strogoff	382	\N	1876	3	4	2018	2
5	rayuela	632	\N	1963	5	5	2013	2
6	perico trepa por chile	182	\N	1978	4	6	2000	2
7	hijo de ladron	312	\N	1951	4	7	2013	2
8	el problema de los tres cuerpos I	1728	\N	2006	8	8	2021	2
9	el coronel no tiene quien le escriba	104	\N	1961	1	9	2003	2
10	la vuelta al mundo en 80 dias	304	\N	1872	3	10	2002	2
11	la vuelta al dia en 80 mundos	214	\N	1967	5	11	2010	2
12	sub terra	84	\N	1904	4	12	2014	2
13	sub sole	94	\N	1907	4	12	2018	2
14	20 poemas de amor y una cancion desesperada	112	\N	1924	4	13	2009	1
15	canto general	468	\N	1950	4	14	2018	1
16	poemas y antipoemas	112	\N	1954	4	15	2001	1
17	mil y una noches	2128	\N	\N	6	16	2021	2
18	guerra y paz	1384	\N	1965	7	17	2015	2
19	el conde de montecristo	1448	1884	1885	3	18	2016	2
\.


--
-- Data for Name: book_author; Type: TABLE DATA; Schema: public; Owner: david
--

COPY biblioteca.book_author (id, book_id, author_id) FROM stdin;
1	1	1
2	2	2
3	3	3
4	4	4
5	5	5
6	6	6
7	6	7
8	7	8
9	8	9
10	9	1
11	10	4
12	11	5
13	12	10
14	13	10
15	14	11
16	15	11
17	16	12
18	17	13
19	18	14
20	19	15
\.


--
-- Data for Name: country; Type: TABLE DATA; Schema: public; Owner: david
--

COPY biblioteca.country (id, name, flag, continent, main_city) FROM stdin;
1	colombia	https://commons.wikimedia.org/wiki/File:Flag_of_Colombia.svg#/media/Archivo:Flag_of_Colombia.svg	América	Bogotá
2	reino unido	https://commons.wikimedia.org/wiki/File:Flag_of_the_United_Kingdom_(3-5).svg#/media/Archivo:Flag_of_the_United_Kingdom_(3-5).svg	Europa	Londres
3	francia	https://commons.wikimedia.org/wiki/File:Flag_of_France_(1794%E2%80%931815,_1830%E2%80%931974).svg#/media/Archivo:Flag_of_France_(1794–1815,_1830–1974).svg	Europa	París
4	chile	https://commons.wikimedia.org/wiki/File:Flag_of_Chile.svg#/media/Archivo:Flag_of_Chile.svg	América	Santiago
5	argentina	https://commons.wikimedia.org/wiki/File:Flag_of_Argentina.svg#/media/Archivo:Flag_of_Argentina.svg	América	Buenos Aires
6	arabia	https://commons.wikimedia.org/wiki/File:Arabian_Peninsula_dust_SeaWiFS.jpg#/media/Archivo:Arabian_Peninsula_dust_SeaWiFS.jpg	Asia	Riad
7	rusia	https://commons.wikimedia.org/wiki/File:Flag_of_Russia.svg#/media/Archivo:Flag_of_Russia.svg	Europa, Asia	Moscú
8	china	https://commons.wikimedia.org/wiki/File:Flag_of_the_People%27s_Republic_of_China.svg#/media/Archivo:Flag_of_the_People's_Republic_of_China.svg	Asia	Pekín
9	españa	https://commons.wikimedia.org/wiki/File:Bandera_de_Espa%C3%B1a.svg#/media/Archivo:Bandera_de_España.svg	Europa	Madrid
\.


--
-- Data for Name: editorial; Type: TABLE DATA; Schema: public; Owner: david
--

COPY biblioteca.editorial (id, name, web, country_id) FROM stdin;
1	contemporanea	https://contemporanea-ediciones.com.ar/	5
2	salamandra	https://www.penguinlibros.com/es/11942-salamandra	9
3	booket	https://www.planeta.es/es/booket	9
4	mestas ediciones	https://mestasediciones.com/	9
5	alfaguara	https://www.penguinlibros.com/es/11579-alfaguara	9
6	sm	https://www.tiendasm.cl/produccion/	4
7	zigzag	https://www.zigzag.cl/	4
8	nova	https://www.penguinlibros.com/es/11773-nova	9
9	debolsillo	https://www.penguinlibros.com/es/11338-debolsillo	9
10	anaya	https://www.grupoanaya.es/	9
11	rm	https://editorialrm.com/	9
12	edición digital	https://ediciondigital.cl/	4
13	edaf	https://www.edaf.net/	9
14	seix barral	https://www.planetadelibros.com/editorial/seix-barral/9	9
15	editorial universitaria	https://www.universitaria.cl/	4
16	longseller	https://longseller.com.ar/	5
17	alianza editorial	https://www.alianzaeditorial.es/	9
18	akal	https://www.akal.com/	9
\.


--
-- Data for Name: style; Type: TABLE DATA; Schema: public; Owner: david
--

COPY biblioteca.style (id, name, description) FROM stdin;
1	poesia	La poesía es un género literario que se caracteriza por el uso de la expresión artística a través del lenguaje, la métrica y la rima. Los poetas utilizan la musicalidad y la metáfora para transmitir emociones, imágenes y significados profundos en un formato compacto y lírico.
2	narrativa	La narrativa es un género literario que se centra en la creación de historias y relatos a través de la palabra escrita. Incluye novelas, cuentos, epopeyas y cualquier forma de escritura que tenga una estructura narrativa. Los narradores pueden ser en tercera persona u omniscientes, primera persona o incluso múltiples voces narrativas.
3	filosofía	En el contexto de géneros literarios, la filosofía se refiere a la escritura filosófica que busca explorar cuestiones fundamentales sobre la existencia, la moral, el conocimiento y la realidad. Los textos filosóficos son discursivos y argumentativos, y los filósofos utilizan la escritura para analizar, debatir y proponer teorías sobre temas abstractos y conceptuales. Los géneros filosóficos incluyen ensayos filosóficos, diálogos socráticos y tratados filosóficos.
\.


--
-- Name: author_id_seq; Type: SEQUENCE SET; Schema: public; Owner: david
--

SELECT pg_catalog.setval('biblioteca.author_id_seq', 15, true);


--
-- Name: book_author_id_seq; Type: SEQUENCE SET; Schema: public; Owner: david
--

SELECT pg_catalog.setval('biblioteca.book_author_id_seq', 20, true);


--
-- Name: books_id_seq; Type: SEQUENCE SET; Schema: public; Owner: david
--

SELECT pg_catalog.setval('biblioteca.books_id_seq', 19, true);


--
-- Name: country_id_seq; Type: SEQUENCE SET; Schema: public; Owner: david
--

SELECT pg_catalog.setval('biblioteca.country_id_seq', 9, true);


--
-- Name: editorial_id_seq; Type: SEQUENCE SET; Schema: public; Owner: david
--

SELECT pg_catalog.setval('biblioteca.editorial_id_seq', 18, true);


--
-- Name: style_id_seq; Type: SEQUENCE SET; Schema: public; Owner: david
--

SELECT pg_catalog.setval('biblioteca.style_id_seq', 3, true);


--
-- Name: author author_pkey; Type: CONSTRAINT; Schema: public; Owner: david
--

ALTER TABLE ONLY biblioteca.author
    ADD CONSTRAINT author_pkey PRIMARY KEY (id);


--
-- Name: book_author book_author_pkey; Type: CONSTRAINT; Schema: public; Owner: david
--

ALTER TABLE ONLY biblioteca.book_author
    ADD CONSTRAINT book_author_pkey PRIMARY KEY (id);


--
-- Name: book books_pkey; Type: CONSTRAINT; Schema: public; Owner: david
--

ALTER TABLE ONLY biblioteca.book
    ADD CONSTRAINT books_pkey PRIMARY KEY (id);


--
-- Name: country country_pkey; Type: CONSTRAINT; Schema: public; Owner: david
--

ALTER TABLE ONLY biblioteca.country
    ADD CONSTRAINT country_pkey PRIMARY KEY (id);


--
-- Name: editorial editorial_pkey; Type: CONSTRAINT; Schema: public; Owner: david
--

ALTER TABLE ONLY biblioteca.editorial
    ADD CONSTRAINT editorial_pkey PRIMARY KEY (id);


--
-- Name: style style_pkey; Type: CONSTRAINT; Schema: public; Owner: david
--

ALTER TABLE ONLY biblioteca.style
    ADD CONSTRAINT style_pkey PRIMARY KEY (id);


--
-- Name: book_author fk_author; Type: FK CONSTRAINT; Schema: public; Owner: david
--

ALTER TABLE ONLY biblioteca.book_author
    ADD CONSTRAINT fk_author FOREIGN KEY (author_id) REFERENCES biblioteca.author(id);


--
-- Name: book_author fk_books; Type: FK CONSTRAINT; Schema: public; Owner: david
--

ALTER TABLE ONLY biblioteca.book_author
    ADD CONSTRAINT fk_books FOREIGN KEY (book_id) REFERENCES biblioteca.book(id);


--
-- Name: editorial fk_country; Type: FK CONSTRAINT; Schema: public; Owner: david
--

ALTER TABLE ONLY biblioteca.editorial
    ADD CONSTRAINT fk_country FOREIGN KEY (country_id) REFERENCES biblioteca.country(id);


--
-- Name: book fk_country; Type: FK CONSTRAINT; Schema: public; Owner: david
--

ALTER TABLE ONLY biblioteca.book
    ADD CONSTRAINT fk_country FOREIGN KEY (country_id) REFERENCES biblioteca.country(id);


--
-- Name: book fk_editorial; Type: FK CONSTRAINT; Schema: public; Owner: david
--

ALTER TABLE ONLY biblioteca.book
    ADD CONSTRAINT fk_editorial FOREIGN KEY (editorial_id) REFERENCES biblioteca.editorial(id);


--
-- Name: book fk_style; Type: FK CONSTRAINT; Schema: public; Owner: david
--

ALTER TABLE ONLY biblioteca.book
    ADD CONSTRAINT fk_style FOREIGN KEY (style_id) REFERENCES biblioteca.style(id);


--
-- PostgreSQL database dump complete
--

--
-- Database "biblioteca_alchemy" dump
--

--
-- PostgreSQL database dump
--

-- Dumped from database version 14.9 (Ubuntu 14.9-0ubuntu0.22.04.1)
-- Dumped by pg_dump version 14.9 (Ubuntu 14.9-0ubuntu0.22.04.1)

SET statement_timeout = 0;
SET lock_timeout = 0;
SET idle_in_transaction_session_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SELECT pg_catalog.set_config('search_path', '', false);
SET check_function_bodies = false;
SET xmloption = content;
SET client_min_messages = warning;
SET row_security = off;

--
-- Name: biblioteca_alchemy; Type: DATABASE; Schema: -; Owner: david
--

CREATE DATABASE biblioteca_alchemy WITH TEMPLATE = template0 ENCODING = 'UTF8' LOCALE = 'es_CL.UTF-8';


ALTER DATABASE biblioteca_alchemy OWNER TO david;

\connect biblioteca_alchemy

SET statement_timeout = 0;
SET lock_timeout = 0;
SET idle_in_transaction_session_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SELECT pg_catalog.set_config('search_path', '', false);
SET check_function_bodies = false;
SET xmloption = content;
SET client_min_messages = warning;
SET row_security = off;

--
-- Name: biblioteca; Type: SCHEMA; Schema: -; Owner: david
--

CREATE SCHEMA biblioteca;


ALTER SCHEMA biblioteca OWNER TO david;

SET default_tablespace = '';

SET default_table_access_method = heap;

--
-- Name: author; Type: TABLE; Schema: biblioteca; Owner: david
--

CREATE TABLE biblioteca.author (
    id integer NOT NULL,
    name character varying(36),
    birth_year integer,
    country_id integer
);


ALTER TABLE biblioteca.author OWNER TO david;

--
-- Name: author_id_seq; Type: SEQUENCE; Schema: biblioteca; Owner: david
--

CREATE SEQUENCE biblioteca.author_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE biblioteca.author_id_seq OWNER TO david;

--
-- Name: author_id_seq; Type: SEQUENCE OWNED BY; Schema: biblioteca; Owner: david
--

ALTER SEQUENCE biblioteca.author_id_seq OWNED BY biblioteca.author.id;


--
-- Name: book; Type: TABLE; Schema: biblioteca; Owner: david
--

CREATE TABLE biblioteca.book (
    id integer NOT NULL,
    name character varying(36),
    pages integer NOT NULL,
    start_year integer,
    end_year integer NOT NULL,
    edicion_year integer NOT NULL,
    country_id integer,
    editorial_id integer,
    style_id integer
);


ALTER TABLE biblioteca.book OWNER TO david;

--
-- Name: book_author; Type: TABLE; Schema: biblioteca; Owner: david
--

CREATE TABLE biblioteca.book_author (
    id integer NOT NULL,
    author_id integer,
    book_id integer
);


ALTER TABLE biblioteca.book_author OWNER TO david;

--
-- Name: book_author_id_seq; Type: SEQUENCE; Schema: biblioteca; Owner: david
--

CREATE SEQUENCE biblioteca.book_author_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE biblioteca.book_author_id_seq OWNER TO david;

--
-- Name: book_author_id_seq; Type: SEQUENCE OWNED BY; Schema: biblioteca; Owner: david
--

ALTER SEQUENCE biblioteca.book_author_id_seq OWNED BY biblioteca.book_author.id;


--
-- Name: book_id_seq; Type: SEQUENCE; Schema: biblioteca; Owner: david
--

CREATE SEQUENCE biblioteca.book_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE biblioteca.book_id_seq OWNER TO david;

--
-- Name: book_id_seq; Type: SEQUENCE OWNED BY; Schema: biblioteca; Owner: david
--

ALTER SEQUENCE biblioteca.book_id_seq OWNED BY biblioteca.book.id;


--
-- Name: country; Type: TABLE; Schema: biblioteca; Owner: david
--

CREATE TABLE biblioteca.country (
    id integer NOT NULL,
    name character varying(36),
    continent character varying(36) NOT NULL
);


ALTER TABLE biblioteca.country OWNER TO david;

--
-- Name: country_id_seq; Type: SEQUENCE; Schema: biblioteca; Owner: david
--

CREATE SEQUENCE biblioteca.country_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE biblioteca.country_id_seq OWNER TO david;

--
-- Name: country_id_seq; Type: SEQUENCE OWNED BY; Schema: biblioteca; Owner: david
--

ALTER SEQUENCE biblioteca.country_id_seq OWNED BY biblioteca.country.id;


--
-- Name: editorial; Type: TABLE; Schema: biblioteca; Owner: david
--

CREATE TABLE biblioteca.editorial (
    id integer NOT NULL,
    name character varying(36),
    web character varying(512),
    country_id integer
);


ALTER TABLE biblioteca.editorial OWNER TO david;

--
-- Name: editorial_id_seq; Type: SEQUENCE; Schema: biblioteca; Owner: david
--

CREATE SEQUENCE biblioteca.editorial_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE biblioteca.editorial_id_seq OWNER TO david;

--
-- Name: editorial_id_seq; Type: SEQUENCE OWNED BY; Schema: biblioteca; Owner: david
--

ALTER SEQUENCE biblioteca.editorial_id_seq OWNED BY biblioteca.editorial.id;


--
-- Name: style; Type: TABLE; Schema: biblioteca; Owner: david
--

CREATE TABLE biblioteca.style (
    id integer NOT NULL,
    name character varying(36),
    description character varying(256)
);


ALTER TABLE biblioteca.style OWNER TO david;

--
-- Name: style_id_seq; Type: SEQUENCE; Schema: biblioteca; Owner: david
--

CREATE SEQUENCE biblioteca.style_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE biblioteca.style_id_seq OWNER TO david;

--
-- Name: style_id_seq; Type: SEQUENCE OWNED BY; Schema: biblioteca; Owner: david
--

ALTER SEQUENCE biblioteca.style_id_seq OWNED BY biblioteca.style.id;


--
-- Name: author id; Type: DEFAULT; Schema: biblioteca; Owner: david
--

ALTER TABLE ONLY biblioteca.author ALTER COLUMN id SET DEFAULT nextval('biblioteca.author_id_seq'::regclass);


--
-- Name: book id; Type: DEFAULT; Schema: biblioteca; Owner: david
--

ALTER TABLE ONLY biblioteca.book ALTER COLUMN id SET DEFAULT nextval('biblioteca.book_id_seq'::regclass);


--
-- Name: book_author id; Type: DEFAULT; Schema: biblioteca; Owner: david
--

ALTER TABLE ONLY biblioteca.book_author ALTER COLUMN id SET DEFAULT nextval('biblioteca.book_author_id_seq'::regclass);


--
-- Name: country id; Type: DEFAULT; Schema: biblioteca; Owner: david
--

ALTER TABLE ONLY biblioteca.country ALTER COLUMN id SET DEFAULT nextval('biblioteca.country_id_seq'::regclass);


--
-- Name: editorial id; Type: DEFAULT; Schema: biblioteca; Owner: david
--

ALTER TABLE ONLY biblioteca.editorial ALTER COLUMN id SET DEFAULT nextval('biblioteca.editorial_id_seq'::regclass);


--
-- Name: style id; Type: DEFAULT; Schema: biblioteca; Owner: david
--

ALTER TABLE ONLY biblioteca.style ALTER COLUMN id SET DEFAULT nextval('biblioteca.style_id_seq'::regclass);


--
-- Data for Name: author; Type: TABLE DATA; Schema: biblioteca; Owner: david
--

COPY biblioteca.author (id, name, birth_year, country_id) FROM stdin;
\.


--
-- Data for Name: book; Type: TABLE DATA; Schema: biblioteca; Owner: david
--

COPY biblioteca.book (id, name, pages, start_year, end_year, edicion_year, country_id, editorial_id, style_id) FROM stdin;
\.


--
-- Data for Name: book_author; Type: TABLE DATA; Schema: biblioteca; Owner: david
--

COPY biblioteca.book_author (id, author_id, book_id) FROM stdin;
\.


--
-- Data for Name: country; Type: TABLE DATA; Schema: biblioteca; Owner: david
--

COPY biblioteca.country (id, name, continent) FROM stdin;
\.


--
-- Data for Name: editorial; Type: TABLE DATA; Schema: biblioteca; Owner: david
--

COPY biblioteca.editorial (id, name, web, country_id) FROM stdin;
\.


--
-- Data for Name: style; Type: TABLE DATA; Schema: biblioteca; Owner: david
--

COPY biblioteca.style (id, name, description) FROM stdin;
\.


--
-- Name: author_id_seq; Type: SEQUENCE SET; Schema: biblioteca; Owner: david
--

SELECT pg_catalog.setval('biblioteca.author_id_seq', 1, false);


--
-- Name: book_author_id_seq; Type: SEQUENCE SET; Schema: biblioteca; Owner: david
--

SELECT pg_catalog.setval('biblioteca.book_author_id_seq', 1, false);


--
-- Name: book_id_seq; Type: SEQUENCE SET; Schema: biblioteca; Owner: david
--

SELECT pg_catalog.setval('biblioteca.book_id_seq', 1, false);


--
-- Name: country_id_seq; Type: SEQUENCE SET; Schema: biblioteca; Owner: david
--

SELECT pg_catalog.setval('biblioteca.country_id_seq', 1, false);


--
-- Name: editorial_id_seq; Type: SEQUENCE SET; Schema: biblioteca; Owner: david
--

SELECT pg_catalog.setval('biblioteca.editorial_id_seq', 1, false);


--
-- Name: style_id_seq; Type: SEQUENCE SET; Schema: biblioteca; Owner: david
--

SELECT pg_catalog.setval('biblioteca.style_id_seq', 1, false);


--
-- Name: author author_name_key; Type: CONSTRAINT; Schema: biblioteca; Owner: david
--

ALTER TABLE ONLY biblioteca.author
    ADD CONSTRAINT author_name_key UNIQUE (name);


--
-- Name: author author_pkey; Type: CONSTRAINT; Schema: biblioteca; Owner: david
--

ALTER TABLE ONLY biblioteca.author
    ADD CONSTRAINT author_pkey PRIMARY KEY (id);


--
-- Name: book_author book_author_pkey; Type: CONSTRAINT; Schema: biblioteca; Owner: david
--

ALTER TABLE ONLY biblioteca.book_author
    ADD CONSTRAINT book_author_pkey PRIMARY KEY (id);


--
-- Name: book book_name_key; Type: CONSTRAINT; Schema: biblioteca; Owner: david
--

ALTER TABLE ONLY biblioteca.book
    ADD CONSTRAINT book_name_key UNIQUE (name);


--
-- Name: book book_pkey; Type: CONSTRAINT; Schema: biblioteca; Owner: david
--

ALTER TABLE ONLY biblioteca.book
    ADD CONSTRAINT book_pkey PRIMARY KEY (id);


--
-- Name: country country_name_key; Type: CONSTRAINT; Schema: biblioteca; Owner: david
--

ALTER TABLE ONLY biblioteca.country
    ADD CONSTRAINT country_name_key UNIQUE (name);


--
-- Name: country country_pkey; Type: CONSTRAINT; Schema: biblioteca; Owner: david
--

ALTER TABLE ONLY biblioteca.country
    ADD CONSTRAINT country_pkey PRIMARY KEY (id);


--
-- Name: editorial editorial_name_key; Type: CONSTRAINT; Schema: biblioteca; Owner: david
--

ALTER TABLE ONLY biblioteca.editorial
    ADD CONSTRAINT editorial_name_key UNIQUE (name);


--
-- Name: editorial editorial_pkey; Type: CONSTRAINT; Schema: biblioteca; Owner: david
--

ALTER TABLE ONLY biblioteca.editorial
    ADD CONSTRAINT editorial_pkey PRIMARY KEY (id);


--
-- Name: style style_name_key; Type: CONSTRAINT; Schema: biblioteca; Owner: david
--

ALTER TABLE ONLY biblioteca.style
    ADD CONSTRAINT style_name_key UNIQUE (name);


--
-- Name: style style_pkey; Type: CONSTRAINT; Schema: biblioteca; Owner: david
--

ALTER TABLE ONLY biblioteca.style
    ADD CONSTRAINT style_pkey PRIMARY KEY (id);


--
-- Name: author author_country_id_fkey; Type: FK CONSTRAINT; Schema: biblioteca; Owner: david
--

ALTER TABLE ONLY biblioteca.author
    ADD CONSTRAINT author_country_id_fkey FOREIGN KEY (country_id) REFERENCES biblioteca.country(id);


--
-- Name: book_author book_author_author_id_fkey; Type: FK CONSTRAINT; Schema: biblioteca; Owner: david
--

ALTER TABLE ONLY biblioteca.book_author
    ADD CONSTRAINT book_author_author_id_fkey FOREIGN KEY (author_id) REFERENCES biblioteca.author(id);


--
-- Name: book_author book_author_book_id_fkey; Type: FK CONSTRAINT; Schema: biblioteca; Owner: david
--

ALTER TABLE ONLY biblioteca.book_author
    ADD CONSTRAINT book_author_book_id_fkey FOREIGN KEY (book_id) REFERENCES biblioteca.book(id);


--
-- Name: book book_country_id_fkey; Type: FK CONSTRAINT; Schema: biblioteca; Owner: david
--

ALTER TABLE ONLY biblioteca.book
    ADD CONSTRAINT book_country_id_fkey FOREIGN KEY (country_id) REFERENCES biblioteca.country(id);


--
-- Name: book book_editorial_id_fkey; Type: FK CONSTRAINT; Schema: biblioteca; Owner: david
--

ALTER TABLE ONLY biblioteca.book
    ADD CONSTRAINT book_editorial_id_fkey FOREIGN KEY (editorial_id) REFERENCES biblioteca.editorial(id);


--
-- Name: book book_style_id_fkey; Type: FK CONSTRAINT; Schema: biblioteca; Owner: david
--

ALTER TABLE ONLY biblioteca.book
    ADD CONSTRAINT book_style_id_fkey FOREIGN KEY (style_id) REFERENCES biblioteca.style(id);


--
-- Name: editorial editorial_country_id_fkey; Type: FK CONSTRAINT; Schema: biblioteca; Owner: david
--

ALTER TABLE ONLY biblioteca.editorial
    ADD CONSTRAINT editorial_country_id_fkey FOREIGN KEY (country_id) REFERENCES biblioteca.country(id);


--
-- PostgreSQL database dump complete
--

--
-- Database "periodico" dump
--

--
-- PostgreSQL database dump
--

-- Dumped from database version 14.9 (Ubuntu 14.9-0ubuntu0.22.04.1)
-- Dumped by pg_dump version 14.9 (Ubuntu 14.9-0ubuntu0.22.04.1)

SET statement_timeout = 0;
SET lock_timeout = 0;
SET idle_in_transaction_session_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SELECT pg_catalog.set_config('search_path', '', false);
SET check_function_bodies = false;
SET xmloption = content;
SET client_min_messages = warning;
SET row_security = off;

--
-- Name: periodico; Type: DATABASE; Schema: -; Owner: perico
--

CREATE DATABASE periodico WITH TEMPLATE = template0 ENCODING = 'UTF8' LOCALE = 'es_CL.UTF-8';


ALTER DATABASE periodico OWNER TO perico;

\connect periodico

SET statement_timeout = 0;
SET lock_timeout = 0;
SET idle_in_transaction_session_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SELECT pg_catalog.set_config('search_path', '', false);
SET check_function_bodies = false;
SET xmloption = content;
SET client_min_messages = warning;
SET row_security = off;

SET default_tablespace = '';

SET default_table_access_method = heap;

--
-- Name: autor; Type: TABLE; Schema: public; Owner: perico
--

CREATE TABLE biblioteca.autor (
    id integer NOT NULL,
    name character varying(256) NOT NULL
);


ALTER TABLE biblioteca.autor OWNER TO perico;

--
-- Name: autor_id_seq; Type: SEQUENCE; Schema: public; Owner: perico
--

CREATE SEQUENCE biblioteca.autor_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE biblioteca.autor_id_seq OWNER TO perico;

--
-- Name: autor_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: perico
--

ALTER SEQUENCE biblioteca.autor_id_seq OWNED BY biblioteca.autor.id;


--
-- Name: libro; Type: TABLE; Schema: public; Owner: perico
--

CREATE TABLE biblioteca.libro (
    id integer NOT NULL,
    name character varying(256) NOT NULL,
    year integer,
    id_author integer
);


ALTER TABLE biblioteca.libro OWNER TO perico;

--
-- Name: libro_id_seq; Type: SEQUENCE; Schema: public; Owner: perico
--

CREATE SEQUENCE biblioteca.libro_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE biblioteca.libro_id_seq OWNER TO perico;

--
-- Name: libro_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: perico
--

ALTER SEQUENCE biblioteca.libro_id_seq OWNED BY biblioteca.libro.id;


--
-- Name: products; Type: TABLE; Schema: public; Owner: perico
--

CREATE TABLE biblioteca.products (
    product_id integer NOT NULL,
    name text,
    price numeric,
    discount_price numeric,
    CONSTRAINT positive_price CHECK ((price > (0)::numeric)),
    CONSTRAINT products_check CHECK ((price > discount_price)),
    CONSTRAINT products_discount_price_check CHECK ((discount_price > (0)::numeric))
);


ALTER TABLE biblioteca.products OWNER TO perico;

--
-- Name: products_product_id_seq; Type: SEQUENCE; Schema: public; Owner: perico
--

CREATE SEQUENCE biblioteca.products_product_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE biblioteca.products_product_id_seq OWNER TO perico;

--
-- Name: products_product_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: perico
--

ALTER SEQUENCE biblioteca.products_product_id_seq OWNED BY biblioteca.products.product_id;


--
-- Name: users; Type: TABLE; Schema: public; Owner: perico
--

CREATE TABLE biblioteca.users (
    "int" integer NOT NULL,
    organization_id integer NOT NULL,
    email character varying(200)
);


ALTER TABLE biblioteca.users OWNER TO perico;

--
-- Name: users_int_seq; Type: SEQUENCE; Schema: public; Owner: perico
--

CREATE SEQUENCE biblioteca.users_int_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE biblioteca.users_int_seq OWNER TO perico;

--
-- Name: users_int_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: perico
--

ALTER SEQUENCE biblioteca.users_int_seq OWNED BY biblioteca.users."int";


--
-- Name: autor id; Type: DEFAULT; Schema: public; Owner: perico
--

ALTER TABLE ONLY biblioteca.autor ALTER COLUMN id SET DEFAULT nextval('biblioteca.autor_id_seq'::regclass);


--
-- Name: libro id; Type: DEFAULT; Schema: public; Owner: perico
--

ALTER TABLE ONLY biblioteca.libro ALTER COLUMN id SET DEFAULT nextval('biblioteca.libro_id_seq'::regclass);


--
-- Name: products product_id; Type: DEFAULT; Schema: public; Owner: perico
--

ALTER TABLE ONLY biblioteca.products ALTER COLUMN product_id SET DEFAULT nextval('biblioteca.products_product_id_seq'::regclass);


--
-- Name: users int; Type: DEFAULT; Schema: public; Owner: perico
--

ALTER TABLE ONLY biblioteca.users ALTER COLUMN "int" SET DEFAULT nextval('biblioteca.users_int_seq'::regclass);


--
-- Data for Name: autor; Type: TABLE DATA; Schema: public; Owner: perico
--

COPY biblioteca.autor (id, name) FROM stdin;
1	Pablo Neruda
2	Gabriel Mistral
3	Nicanor Parra
\.


--
-- Data for Name: libro; Type: TABLE DATA; Schema: public; Owner: perico
--

COPY biblioteca.libro (id, name, year, id_author) FROM stdin;
1	20 poemas de amor y una conción desesperada	1970	1
2	Desolación	1922	2
\.


--
-- Data for Name: products; Type: TABLE DATA; Schema: public; Owner: perico
--

COPY biblioteca.products (product_id, name, price, discount_price) FROM stdin;
2	alfajor	800	500
\.


--
-- Data for Name: users; Type: TABLE DATA; Schema: public; Owner: perico
--

COPY biblioteca.users ("int", organization_id, email) FROM stdin;
1	1	escribe@gmail.com
3	1	secretario@gmail.com
4	1	tesorero@gmail.com
\.


--
-- Name: autor_id_seq; Type: SEQUENCE SET; Schema: public; Owner: perico
--

SELECT pg_catalog.setval('biblioteca.autor_id_seq', 3, true);


--
-- Name: libro_id_seq; Type: SEQUENCE SET; Schema: public; Owner: perico
--

SELECT pg_catalog.setval('biblioteca.libro_id_seq', 2, true);


--
-- Name: products_product_id_seq; Type: SEQUENCE SET; Schema: public; Owner: perico
--

SELECT pg_catalog.setval('biblioteca.products_product_id_seq', 2, true);


--
-- Name: users_int_seq; Type: SEQUENCE SET; Schema: public; Owner: perico
--

SELECT pg_catalog.setval('biblioteca.users_int_seq', 4, true);


--
-- Name: autor autor_pkey; Type: CONSTRAINT; Schema: public; Owner: perico
--

ALTER TABLE ONLY biblioteca.autor
    ADD CONSTRAINT autor_pkey PRIMARY KEY (id);


--
-- Name: libro libro_pkey; Type: CONSTRAINT; Schema: public; Owner: perico
--

ALTER TABLE ONLY biblioteca.libro
    ADD CONSTRAINT libro_pkey PRIMARY KEY (id);


--
-- Name: users org_email; Type: CONSTRAINT; Schema: public; Owner: perico
--

ALTER TABLE ONLY biblioteca.users
    ADD CONSTRAINT org_email UNIQUE (organization_id, email);


--
-- Name: products products_pkey; Type: CONSTRAINT; Schema: public; Owner: perico
--

ALTER TABLE ONLY biblioteca.products
    ADD CONSTRAINT products_pkey PRIMARY KEY (product_id);


--
-- Name: users users_pkey; Type: CONSTRAINT; Schema: public; Owner: perico
--

ALTER TABLE ONLY biblioteca.users
    ADD CONSTRAINT users_pkey PRIMARY KEY ("int");


--
-- Name: libro fk_id_author; Type: FK CONSTRAINT; Schema: public; Owner: perico
--

ALTER TABLE ONLY biblioteca.libro
    ADD CONSTRAINT fk_id_author FOREIGN KEY (id_author) REFERENCES biblioteca.autor(id);


--
-- PostgreSQL database dump complete
--

--
-- Database "postgres" dump
--

\connect postgres

--
-- PostgreSQL database dump
--

-- Dumped from database version 14.9 (Ubuntu 14.9-0ubuntu0.22.04.1)
-- Dumped by pg_dump version 14.9 (Ubuntu 14.9-0ubuntu0.22.04.1)

SET statement_timeout = 0;
SET lock_timeout = 0;
SET idle_in_transaction_session_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SELECT pg_catalog.set_config('search_path', '', false);
SET check_function_bodies = false;
SET xmloption = content;
SET client_min_messages = warning;
SET row_security = off;

--
-- PostgreSQL database dump complete
--

--
-- PostgreSQL database cluster dump complete
--

