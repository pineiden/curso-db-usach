\connect biblioteca


--
-- Data for Name: author; Type: TABLE DATA; Schema: public; Owner: martin
--

COPY biblioteca.author (id, name, birth_year) FROM stdin;
1	gabriel garcia marquez	1927
2	jk rowling	1965
3	jrr tolkien	1892
4	julio verne	1828
5	julio cortazar	1914
6	alicia morel	1921
7	marcela paz	1902
8	manuel rojas	1896
9	liu cixin	1963
10	baldomero lillo	1867
11	pablo neruda	1904
12	nicanor parra	1914
13	anonimo	\N
14	leon tolstoi	1828
15	alejandro dumas	1802
\.


--
-- Data for Name: country; Type: TABLE DATA; Schema: public; Owner: martin
--

COPY biblioteca.country (id, name, flag, continent, main_city) FROM stdin;
1	colombia	https://commons.wikimedia.org/wiki/File:Flag_of_Colombia.svg#/media/Archivo:Flag_of_Colombia.svg	América	Bogotá
2	reino unido	https://commons.wikimedia.org/wiki/File:Flag_of_the_United_Kingdom_(3-5).svg#/media/Archivo:Flag_of_the_United_Kingdom_(3-5).svg	Europa	Londres
3	francia	https://commons.wikimedia.org/wiki/File:Flag_of_France_(1794%E2%80%931815,_1830%E2%80%931974).svg#/media/Archivo:Flag_of_France_(1794–1815,_1830–1974).svg	Europa	París
4	chile	https://commons.wikimedia.org/wiki/File:Flag_of_Chile.svg#/media/Archivo:Flag_of_Chile.svg	América	Santiago
5	argentina	https://commons.wikimedia.org/wiki/File:Flag_of_Argentina.svg#/media/Archivo:Flag_of_Argentina.svg	América	Buenos Aires
6	arabia	https://commons.wikimedia.org/wiki/File:Arabian_Peninsula_dust_SeaWiFS.jpg#/media/Archivo:Arabian_Peninsula_dust_SeaWiFS.jpg	Asia	Riad
7	rusia	https://commons.wikimedia.org/wiki/File:Flag_of_Russia.svg#/media/Archivo:Flag_of_Russia.svg	Europa, Asia	Moscú
8	china	https://commons.wikimedia.org/wiki/File:Flag_of_the_People%27s_Republic_of_China.svg#/media/Archivo:Flag_of_the_People's_Republic_of_China.svg	Asia	Pekín
9	españa	https://commons.wikimedia.org/wiki/File:Bandera_de_Espa%C3%B1a.svg#/media/Archivo:Bandera_de_España.svg	Europa	Madrid
\.


--
-- Data for Name: editorial; Type: TABLE DATA; Schema: public; Owner: martin
--

COPY biblioteca.editorial (id, name, web, country_id) FROM stdin;
1	contemporanea	https://contemporanea-ediciones.com.ar/	5
2	salamandra	https://www.penguinlibros.com/es/11942-salamandra	9
3	booket	https://www.planeta.es/es/booket	9
4	mestas ediciones	https://mestasediciones.com/	9
5	alfaguara	https://www.penguinlibros.com/es/11579-alfaguara	9
6	sm	https://www.tiendasm.cl/produccion/	4
7	zigzag	https://www.zigzag.cl/	4
8	nova	https://www.penguinlibros.com/es/11773-nova	9
9	debolsillo	https://www.penguinlibros.com/es/11338-debolsillo	9
10	anaya	https://www.grupoanaya.es/	9
11	rm	https://editorialrm.com/	9
12	edición digital	https://ediciondigital.cl/	4
13	edaf	https://www.edaf.net/	9
14	seix barral	https://www.planetadelibros.com/editorial/seix-barral/9	9
15	editorial universitaria	https://www.universitaria.cl/	4
16	longseller	https://longseller.com.ar/	5
17	alianza editorial	https://www.alianzaeditorial.es/	9
18	akal	https://www.akal.com/	9
\.


--
-- Data for Name: style; Type: TABLE DATA; Schema: public; Owner: martin
--

COPY biblioteca.style (id, name, description) FROM stdin;
1	poesia	La poesía es un género literario que se caracteriza por el uso de la expresión artística a través del lenguaje, la métrica y la rima. Los poetas utilizan la musicalidad y la metáfora para transmitir emociones, imágenes y significados profundos en un formato compacto y lírico.
2	narrativa	La narrativa es un género literario que se centra en la creación de historias y relatos a través de la palabra escrita. Incluye novelas, cuentos, epopeyas y cualquier forma de escritura que tenga una estructura narrativa. Los narradores pueden ser en tercera persona u omniscientes, primera persona o incluso múltiples voces narrativas.
3	filosofía	En el contexto de géneros literarios, la filosofía se refiere a la escritura filosófica que busca explorar cuestiones fundamentales sobre la existencia, la moral, el conocimiento y la realidad. Los textos filosóficos son discursivos y argumentativos, y los filósofos utilizan la escritura para analizar, debatir y proponer teorías sobre temas abstractos y conceptuales. Los géneros filosóficos incluyen ensayos filosóficos, diálogos socráticos y tratados filosóficos.
\.


--
-- Data for Name: book; Type: TABLE DATA; Schema: public; Owner: martin
--

COPY biblioteca.book (id, name, pages, start_year, end_year, country_id, editorial_id, edition, style_id) FROM stdin;
1	cien años de soledad	471	\N	1982	1	1	2003	2
2	harry potter	3792	\N	1997	2	2	2020	2
3	el señor de los anillos, la comunidad del anillo	1392	\N	1954	2	3	2016	2
4	miguel strogoff	382	\N	1876	3	4	2018	2
5	rayuela	632	\N	1963	5	5	2013	2
6	perico trepa por chile	182	\N	1978	4	6	2000	2
7	hijo de ladron	312	\N	1951	4	7	2013	2
8	el problema de los tres cuerpos I	1728	\N	2006	8	8	2021	2
9	el coronel no tiene quien le escriba	104	\N	1961	1	9	2003	2
10	la vuelta al mundo en 80 dias	304	\N	1872	3	10	2002	2
11	la vuelta al dia en 80 mundos	214	\N	1967	5	11	2010	2
12	sub terra	84	\N	1904	4	12	2014	2
13	sub sole	94	\N	1907	4	12	2018	2
14	20 poemas de amor y una cancion desesperada	112	\N	1924	4	13	2009	1
15	canto general	468	\N	1950	4	14	2018	1
16	poemas y antipoemas	112	\N	1954	4	15	2001	1
17	mil y una noches	2128	\N	\N	6	16	2021	2
18	guerra y paz	1384	\N	1965	7	17	2015	2
19	el conde de montecristo	1448	1884	1885	3	18	2016	2
\.


--
-- Data for Name: book_author; Type: TABLE DATA; Schema: public; Owner: martin
--

COPY biblioteca.book_author (id, book_id, author_id) FROM stdin;
1	1	1
2	2	2
3	3	3
4	4	4
5	5	5
6	6	6
7	6	7
8	7	8
9	8	9
10	9	1
11	10	4
12	11	5
13	12	10
14	13	10
15	14	11
16	15	11
17	16	12
18	17	13
19	18	14
20	19	15
\.


--
-- Name: author_id_seq; Type: SEQUENCE SET; Schema: public; Owner: martin
--

SELECT pg_catalog.setval('biblioteca.author_id_seq', 15, true);


--
-- Name: book_author_id_seq; Type: SEQUENCE SET; Schema: public; Owner: martin
--

SELECT pg_catalog.setval('biblioteca.book_author_id_seq', 20, true);


--
-- Name: books_id_seq; Type: SEQUENCE SET; Schema: public; Owner: martin
--

SELECT pg_catalog.setval('biblioteca.book_id_seq', 19, true);


--
-- Name: country_id_seq; Type: SEQUENCE SET; Schema: public; Owner: martin
--

SELECT pg_catalog.setval('biblioteca.country_id_seq', 9, true);


--
-- Name: editorial_id_seq; Type: SEQUENCE SET; Schema: public; Owner: martin
--

SELECT pg_catalog.setval('biblioteca.editorial_id_seq', 18, true);


--
-- Name: style_id_seq; Type: SEQUENCE SET; Schema: public; Owner: martin
--

SELECT pg_catalog.setval('biblioteca.style_id_seq', 3, true);


