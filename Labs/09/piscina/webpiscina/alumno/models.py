from django.db import models
from django.contrib.auth.models import User
from datetime import date, datetime
from django.utils.translation import gettext as _

# Create your models here.
class Alumno(models.Model):
    birth_date = models.DateField(
        auto_now=False,
        unique=False,
        blank=False
    )
    user = models.ForeignKey(
        User, 
        on_delete=models.PROTECT)
    es_alumno = models.BooleanField(
        default=False)
    """
    1) alumno no puede ser profesor

    """
    def __repr__(self):
        return f"Alumno({self.nombre})"

    def __str__(self):
        return f"{self.nombre}"


    @property
    def edad(self):
        return int((date.today()-self.birth_date).days/365.2425)

    @property
    def nombre(self):
        
        return f"{self.user.first_name} {self.user.last_name}"

    @property
    def info(self):
        return {
            "nombre":self.nombre,
            "email":self.user.email,
            "active": self.user.is_active
            
        }


    class Meta:
        verbose_name_plural = _("Alumnos")
