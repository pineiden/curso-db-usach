from django.db import models
from django.contrib.auth.models import User
from datetime import datetime, date
from django.utils.translation import gettext as _
import unicodedata
import string
from django.core.exceptions import ValidationError

def clean_string(text):
    return "".join([c for c in unicodedata.normalize('NFD',text) if c in string.ascii_letters])

def validate_name(user):
    username = clean_string(f"{user.first_name}{user.last_name}")
    if not username==user.username:
        raise ValidationError(_(f"""{username} not valid for profesor,
        change to first_name_last_name"""))

# Create your models here.
class Profesor(models.Model):
    birth_date = models.DateField(
        auto_now=False,
        unique=False,
        blank=False
    )
    user = models.ForeignKey(
        User, 
        on_delete=models.PROTECT, 
        validators=[validate_name])
    es_profesor = models.BooleanField(
        default=True)
    """
    1)  profesor

    """

    def __repr__(self):
        return f"Profesor({self.nombre})"

    def __str__(self):
        return f"{self.nombre}"

    @property
    def edad(self):
        return int((date.today()-self.birth_date).days/365.2425)

    @property
    def nombre(self):
        
        return f"{self.user.first_name} {self.user.last_name}"

    @property
    def info(self):
        return {
            "nombre":self.nombre,
            "email":self.user.email,
            "active": self.user.is_active
            
        }


    class Meta:
        verbose_name_plural = _("Profesores")


    def save(self, *args, **kwargs): 
        # Normalizar y convertir a minúsculas antes de guardar 
        validate_name(self.user)
        super().save(*args, **kwargs)
