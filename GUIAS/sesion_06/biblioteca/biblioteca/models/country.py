from sqlmodel import Field, SQLModel


class Country(SQLModel, table=True):
    id: int = Field(default=None, primary_key=True)
    name: str
    continent: str


    def __repr__(self):
        return f"Country({self.name}, {self.contintent})"

    def __str__(self):
        return f"Country({self.name}, {self.continent})"

    
