from sqlmodel import Field, SQLModel


class BookAuthor(SQLModel, table=True):
    id: int = Field(default=None, primary_key=True)

    author_id: int|None = Field(
        default=None, 
        foreign_key="author.id")

    book_id: int|None = Field(
        default=None, 
        foreign_key="book.id")

    def __repr__(self):
        return f"BookAuthor({self.author_id},{self.book_id})"

    def __str__(self):
        return f"BookAuthor({self.author_id},{self.book_id})"
