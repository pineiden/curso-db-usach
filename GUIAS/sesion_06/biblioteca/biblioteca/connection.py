from sqlmodel import Session, create_engine
import pandas as pd
from sqlalchemy import text
from sqlalchemy.orm import sessionmaker

class BibliotecaSession:
    # método constructor (self, lista expandida: *args, diccionario
    # expandido: **kwargs)
    def __init__(self, *args, **kwargs):
        self.db_path = 'postgresql://{dbuser}:{dbpass}@{dbhost}:{dbport}/{dbname}'.format(**kwargs)

        # esto genera un cliente al postgres
        self.db_engine = create_engine(
            self.db_path, 
            echo=True)
        self.session = sessionmaker(
            self.db_engine, 
            expire_on_commit=False)
        self.db_data = kwargs

    def execute(self, sql:str):
        """Execute SQL"""
        print("Query to run",sql)
        with self.db_engine.connect() as conn:
            conn.execute(text(sql))

    def retrieve(self, sql:str):
        """
        Execute SQL and retrieve results
        """
        with self.db_engine.connect() as conn:
            df = pd.read_sql(text(sql), conn)
            return df



if __name__=="__main__":
    params = {
        "dbuser":"david",
        "dbpass":"oxido",
        "dbhost":"localhost",
        "dbport":5432,
        "dbname":"biblioteca_orm"
    }
    conn = BibliotecaSession(**params)

    query_schema = """
SELECT table_name
FROM information_schema.tables
WHERE table_schema='public'
AND table_type='BASE TABLE';
"""
    df = conn.retrieve(query_schema)

    print("Conexión", conn)

    print(df)
