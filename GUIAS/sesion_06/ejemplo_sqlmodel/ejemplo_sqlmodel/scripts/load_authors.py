import typer
from pathlib import Path
import csv
from sqlmodel import create_engine, Session
from ejemplo_sqlmodel.entities import Author


app = typer.Typer()

def to_number(fn, value:str)->int|None:
    if value=='':
        return None
    else:
        return fn(value)

@app.command()
def load_authors(database:str, path:Path):
    #levantamos session a db
    engine = create_engine(f"sqlite:///{database}.db")
    # leemos csv
    columns = {
        "Nombre":("name", str),
        "Año nacimiento": ("birth_year", int)
    }
    if path.exists():
        with path.open() as f:
            with Session(engine) as session:
                reader = csv.DictReader(f)
                for item in reader:
                    item.pop('id')
                    new_item = {columns[k][0]:to_number(columns[k][1],v) for k,v in
                                item.items()}
                    print(new_item)
                    new_author = Author(**new_item)
                    session.add(new_author)
                session.commit()

    else:
        print(f"No existe el path {path}")


if __name__=="__main__":
    app()
