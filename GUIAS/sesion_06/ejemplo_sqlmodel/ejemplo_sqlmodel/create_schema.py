from ejemplo_sqlmodel.entities import Base
from sqlmodel import create_engine
import typer


app = typer.Typer()

@app.command()
def build_schema(name:str="biblioteca"):
    engine = create_engine(f"sqlite:///{name}.db")
    Base.metadata.create_all(engine)


if __name__=="__main__":
    app()
