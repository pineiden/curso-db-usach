from typing import Optional
from sqlmodel import Field, Session, SQLModel, create_engine, Relationship
from .base import Base

class BookAuthor(Base, table=True):
    id: Optional[int] = Field(
        default=None, 
        primary_key=True)

    book_id: int|None =Field(
        default=None, 
        foreign_key="book.id")

    author_id: int|None =Field(
        default=None, 
        foreign_key="author.id")

    authors: list["Author"] = Relationship(
        back_populates="author_books")


    books: list["Book"] = Relationship(
        back_populates="book_authors")
