from typing import Optional
from sqlmodel import Field, Session, SQLModel, create_engine, Relationship
import enum
from .base import Base


class Continent(enum.StrEnum):
    AMERICA= 'América'
    AFRICA = 'África'
    EUROPA = 'Europa'
    ASIA = 'Asia'
    OCEANIA = 'Oceanía'
    ANTARTICA = 'Antártica'

class Country(Base, table=True):
    id: Optional[int] = Field(
        default=None, 
        primary_key=True)
    name: str
    flag: str
    continent: Continent
    main_city: str


    # books: list["Book"] = Relationship(
    #     back_populates="countries")

