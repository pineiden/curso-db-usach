from typing import Optional
from sqlmodel import Field, Session, SQLModel, create_engine, Relationship
from .base import Base
from .book_author import BookAuthor

class Author(Base, table=True):
    id: Optional[int] = Field(
        default=None, 
        primary_key=True)
    name: str
    birth_year: int|None

    author_books: BookAuthor|None = Relationship(
        back_populates="authors")

