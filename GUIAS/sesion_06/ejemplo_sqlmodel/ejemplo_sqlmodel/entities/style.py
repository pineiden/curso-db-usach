from typing import Optional
from sqlmodel import Field, Session, create_engine, Relationship
from .base import Base


class Style(Base, table=True):
    id: Optional[int] = Field(
        default=None, 
        primary_key=True)
    name: str
    description: str


    # books: list["Book"] = Relationship(
    #     back_populates="countries")
