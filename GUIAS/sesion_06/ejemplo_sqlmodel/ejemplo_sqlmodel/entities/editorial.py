from typing import Optional
from sqlmodel import Field, Session, SQLModel, create_engine, Relationship
from .base import Base


class Editorial(Base, table=True):
    id: Optional[int] = Field(
        default=None, 
        primary_key=True)
    name: str
    web: str
    country_id: int|None=Field(
        default=None, 
        foreign_key="country.id")

    # books: list["Book"] = Relationship(
    #     back_populates="countries")
