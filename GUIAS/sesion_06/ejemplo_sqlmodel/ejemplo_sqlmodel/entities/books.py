from typing import Optional
from .style import Style
from .country import Country
from .editorial import Editorial
from sqlmodel import Field, Relationship, SQLModel
from .base import Base
from .book_author import BookAuthor


class Book(Base, table=True):
    id: Optional[int] = Field(
        default=None, 
        primary_key=True)
    name:str
    pages:int
    start_year:int
    end_year:int
    edition:int

    country_id: int|None = Field(
        default=None, 
        foreign_key="country.id")
    editorial_id: int|None = Field(
        default=None,
        foreign_key="editorial.id"        
    )
    style_id: int|None =  Field(
        default=None,
        foreign_key="style.id"        
    )

    book_authors: BookAuthor|None = Relationship(
        back_populates="books")


