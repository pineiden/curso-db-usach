from biblioteca.models import (Country, Style, Editorial, Author,
                               Book, BookAuthor)
from sqlalchemy import text
from biblioteca.sesion import BibliotecaSession
from sqlalchemy.orm import Session
from sqlalchemy import select


def create_session():
    try:
        params = {
            "dbname": "biblioteca",
            "dbuser": "david",
            "dbpass": "oxido",
            "dbhost": "localhost",
            "dbport": "5432"
        }
        # print("Session")
        # print(BibliotecaSession)
        bibcon = BibliotecaSession(**params)
        # print(bibcon)
        #result = bibcon.session.execute(text("select * from books"))
        # print(result)
        # for item in result:
        #    print(item)
        return bibcon
    except Exception as e:
        raise e


sesion = create_session()
session = Session(sesion.engine)

stmt = select(Book).join(Style).where(
    Book.edicion_year >= 1955)

print("Books")
# transacciones -> sql
with Session(sesion.engine) as session:
    for row in session.execute(stmt):
        book = row[0]
        print(book, type(book), book.country)
        #book.country_id = 2
    # session.commit()

    editorial = session.get(Editorial, {"id": 2})
    print(editorial)
