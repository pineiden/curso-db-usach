from django.db import models

# Create your models here.
class Tarea(models.Model):
    name = models.CharField(max_length=256)
    start = models.DateTimeField(auto_now=False)
    duration = models.IntegerField()
    aplication = models.CharField(max_length=256)

    class Meta:
        verbose_name_plural = "Tareas"
        

    def __repr__(self):
        return f"Tarea({self.name}, {self.start}, {self.duration})"
    

    def __str__(self):
        return f"Tarea({self.name}, {self.start}, {self.duration})"
    

