from django.core.management.base import BaseCommand, CommandError
from tarea.models import Tarea
import random
from datetime import datetime, timedelta, timezone
from rich import print


class Command(BaseCommand):
    help = "Crea muchos datos de Tarea dado un entero N>0"

    def add_arguments(self, parser):
        parser.add_argument("cantidad", type=int)
        parser.add_argument("duracion_minima", default=0, type=int)
        parser.add_argument("duracion_maxima", default=100, type=int)

    
    # una función genérica, que permita una cantidad indeterminada de
    # argumentos, y argumentos con llaves
    # expansión de lista
    # [a,b,c]
    # handle(*[a,b,c]) == handle(a,b,c)
    # expansión de diccionario
    # params = {a:1,b:2,c:3} 
    # handle(**parmas) = handle(a=1,b=2,c=3)
    def handle(self, *args, **kwargs):
        cantidad = kwargs.get("cantidad")
        max_duracion = kwargs.get("duracion_maxima")
        min_duracion = kwargs.get("duracion_minima")

        print(f"Comando para generar datos -> {cantidad} muestras")
        
        aplicaciones = [
            "desktop",
            "tablet",
            "servidor",
            "celular",
            "plc"            
        ]

        nombre_tareas = [
            "iniciar_app",
            "limpiar_cache",
            "extraer_data",
            "recargar_vista",
            "borrar_data",
            "enviar_email"
        ]

        year = lambda: random.choice([i for i in range(2020,2026)])
        month = lambda: random.choice([i for i in range(1,13)])
        days = lambda: random.choice([i for i in range(1,32)])
        hours = lambda: random.choice([i for i in range(1,24)])

        for i in range(cantidad):
            duracion = random.randint(min_duracion,max_duracion)
            start_naive = datetime(year(), month(), 1) + timedelta(
                days=days(),hours=hours()
            )
            start_aware = start_naive.replace(tzinfo=timezone.utc)
            parametros = {
                "name": random.choice(nombre_tareas),
                "aplication":random.choice(aplicaciones),
                "start": start_aware,
                "duration": duracion
            }
            tarea = Tarea(**parametros)
            tarea.save()
