from django.core.management.base import BaseCommand, CommandError
from task.models import Task
import random
from datetime import datetime, timedelta
import pytz

class Command(BaseCommand):
    help = "Create samples of tasks"

    def add_arguments(self, parser):
        parser.add_argument("amount", type=int)
        parser.add_argument("min_duration", default=0, type=int)
        parser.add_argument("max_duration", default=100, type=int)


    def handle(self, *args, **kwargs):
        task_names = [
            "start_app",
            "clear_cache",
            "fetch_data",
            "reload_view",
            "delete_data",
            "send_email"
        ]
        application = [
            "desktop",
            "server",
            "smartphone",
            "raspberry"
        ]
        amount = kwargs["amount"]
        max_amount = kwargs["max_duration"]
        min_amount = kwargs["min_duration"]
        year = lambda: random.choice(list(range(2020, 2024)))
        month = lambda: random.choice(list(range(1,13)))
        days = lambda: random.choice(list(range(1,32)))
        hours = lambda: random.choice(list(range(1,24)))
        print("AMount", min_amount, max_amount)
        for i in range(amount):
            duration = random.randint(min_amount, max_amount)
            start = datetime(year(), month(),
                               1)+timedelta(
                                   days=days(), 
                                   hours=hours())
            start = pytz.utc.localize(start)
            task = Task(
                name=random.choice(task_names),
                application=random.choice(application),
                start=start,
                duration=duration
            )
            task.save()
            print(task)
