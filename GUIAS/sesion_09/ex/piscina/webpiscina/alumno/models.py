from django.db import models
from django.contrib.auth.models import User
from datetime import date, datetime


# Create your models here.
class Alumno(models.Model):
    birth_date = models.DateField(
        auto_now=False,
        unique=False,
        blank=False
    )
    user = models.ForeignKey(
        User, 
        on_delete=models.PROTECT)
    es_alumno = models.BooleanField(
        default=False)
    """
    1) alumno no puede ser profesor

    """

    @property
    def edad(self):
        return int((date.today()-self.birth_date).days/365.2425)

    @property
    def nombre(self):
        
        return f"{self.user.first_name} {self.user.last_name}"

    @property
    def info(self):
        return {
            "nombre":self.nombre,
            "email":self.user.email,
            "active": self.user.is_active
            
        }
