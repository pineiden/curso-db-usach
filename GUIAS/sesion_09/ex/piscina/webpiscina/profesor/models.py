from django.db import models
from django.contrib.auth.models import User
from datetime import datetime, date


# Create your models here.
class Profesor(models.Model):
    birth_date = models.DateField(
        auto_now=False,
        unique=False,
        blank=False
    )
    user = models.ForeignKey(
        User, 
        on_delete=models.PROTECT)
    es_profesor = models.BooleanField(
        default=True)
    """
    1)  profesor

    """

    @property
    def edad(self):
        return int((date.today()-self.birth_date).days/365.2425)

    @property
    def nombre(self):
        
        return f"{self.user.first_name} {self.user.last_name}"

    @property
    def info(self):
        return {
            "nombre":self.nombre,
            "email":self.user.email,
            "active": self.user.is_active
            
        }
