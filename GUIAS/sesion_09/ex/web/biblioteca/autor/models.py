from django.db import models
from pais.models import Pais

# Create your models here.

class Autor(models.Model):
    AC_DC = {
        "AC":"Antes de Cristo",
        "DC":"Después de Cristo"
    }

    nombre = models.CharField(max_length=256)
    año_nacimiento = models.PositiveIntegerField()
    ac_dc_inicio = models.CharField(max_length=2, default="DC", choices=AC_DC)
    pais = models.ForeignKey(Pais, on_delete=models.SET_NULL, null=True)


    def __str__(self):
        return f"Autor({self.nombre}, {self.año_nacimiento}, {self.ac_dc_inicio})"

    def __repr__(self):
        return f"Autor({self.nombre}, {self.año_nacimiento}, {self.ac_dc_inicio})"

    class Meta:
        verbose_name_plural = "autores"
