from django.db import models

# Create your models here.

class Pais(models.Model):
    CONTINENTES = {
        "Asia": "Asia",
        "Europa": "Europa",
        "América":"América",
        "Oceanía": "Oceania",
        "Antártica": "Antártica"
    }
    nombre = models.CharField(max_length=128)
    continente = models.CharField(
        max_length=32, 
        default="America", 
        choices=CONTINENTES)
    bandera = models.ImageField(upload_to="banderas/",null=True, blank=True)

    def __str__(self):
        return f"Pais({self.nombre}, {self.continente})"


    class Meta:
        verbose_name_plural = "países"
