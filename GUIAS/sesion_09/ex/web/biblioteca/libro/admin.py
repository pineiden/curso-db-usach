from django.contrib import admin
from .models import Libro

# Register your models here.

class LibroAdmin(admin.ModelAdmin):
    pass

admin.site.register(Libro,LibroAdmin)
