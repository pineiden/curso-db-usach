from django.db import models
from autor.models import Autor

# Create your models here.
class Libro(models.Model):
    AC_DC = {
        "AC":"Antes de Cristo",
        "DC":"Después de Cristo"
    }
    nombre = models.CharField(max_length=256)
    paginas = models.PositiveIntegerField()
    año_inicio = models.PositiveIntegerField()
    ac_dc_inicio = models.CharField(max_length=2, default="DC", choices=AC_DC)
    año_final = models.PositiveIntegerField()
    ac_dc_final = models.CharField(max_length=2, default="DC", choices=AC_DC)
    edicion = models.PositiveIntegerField()
    ac_dc_edicion = models.CharField(max_length=2, default="DC", choices=AC_DC)
    autor = models.ManyToManyField(
        Autor,
        default="Desconocido")

    def __str__(self):
        return f"Libro({self.nombre}, {self.edicion} - {self.ac_dc_edicion})"
