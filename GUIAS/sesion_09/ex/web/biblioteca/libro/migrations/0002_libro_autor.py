# Generated by Django 5.0.6 on 2024-06-04 00:55

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('autor', '0002_alter_autor_options_autor_pais'),
        ('libro', '0001_initial'),
    ]

    operations = [
        migrations.AddField(
            model_name='libro',
            name='autor',
            field=models.ManyToManyField(default='Desconocido', to='autor.autor'),
        ),
    ]
