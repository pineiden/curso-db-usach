from django.db import models
from django.utils.translation import gettext_lazy as _
# Create your models here.

class Country(models.Model):

    class Continent(models.TextChoices):
        AMERICA = "AM", _("America")
        EUROPE  = "EU", _("Europe")
        AFRICA  = "AF", _("Africa")
        ASIA    = "AS", _("Asia")
        OCEANIA = "OC", _("Oceania")


    name = models.CharField(max_length=128)
    flag  = models.URLField()
    continent = models.CharField(
        max_length=2,
        choices=Continent.choices, 
        default=Continent.AMERICA)


    class Meta:
        verbose_name_plural = "Countries"
