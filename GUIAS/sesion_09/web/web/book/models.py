from django.db import models
from country.models import Country

# Create your models here.
class Book(models.Model):
    name = models.CharField(max_length=512)
    pages = models.IntegerField()
    start_year = models.IntegerField()
    end_year = models.IntegerField()
    edition_year = models.IntegerField()
    country_id = models.ForeignKey(
        Country, 
        null=True, 
        blank=True, 
        on_delete=models.PROTECT)
