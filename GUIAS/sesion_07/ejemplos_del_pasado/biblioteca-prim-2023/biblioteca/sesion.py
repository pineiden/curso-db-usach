from sqlalchemy.orm import sessionmaker
from sqlalchemy import create_engine
from sqlalchemy.sql import text


class BibliotecaSession:
    
    def __init__(self, *args, **kwargs):
        self.db_engine = 'postgresql://{dbuser}:{dbpass}@{dbhost}:{dbport}/{dbname}'.format(
            **kwargs)
        self.engine = create_engine(self.db_engine)
        self.connection = self.engine.connect()
        self.session = sessionmaker(bind=self.engine)()
        self.data = kwargs
        self.active_session = None

    def get_session(self):
        return self.session

    def run_sql(self, sql):
        if isinstance(sql, str):
            sql = text(sql)
        self.connection.execute(sql)


    def __enter__(self):
        self.active_session = self.session.begin()
        return self.active_session

    def __exit__(self, exc_type, exc_val, exc_tb):
        print('leaving', self.session)
        if self.active_session:
            self.active_session.commit()
            self.active_session.close()
