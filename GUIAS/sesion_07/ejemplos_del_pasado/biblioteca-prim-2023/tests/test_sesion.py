

from biblioteca.sesion import BibliotecaSession
from sqlalchemy import text


def main():
    try:
        params = {
            "dbname": "biblioteca",
            "dbuser": "david",
            "dbpass": "oxido",
            "dbhost": "localhost",
            "dbport": "5432"
        }
        print("Session")
        print(BibliotecaSession)
        bibcon = BibliotecaSession(**params)
        print(bibcon)
        result = bibcon.session.execute(text("select * from books"))
        print(result)
        for item in result:
            print(item)
    except Exception as e:
        raise e


if __name__ == "__main__":
    main()
