from biblioteca.models import  (
    Country, 
    Style, 
    Editorial, 
    Author,
    Book, 
    BookAuthor)
from biblioteca.connection import BibliotecaSession
from pydantic import BaseModel
from sqlmodel import Session, select
from typing import Dict, Any
from sqlalchemy.orm import sessionmaker

class ManagerBiblioteca:
    session:BibliotecaSession
    
    def __init__(self, session:BibliotecaSession):
        self.session = session
    
    def create_country(self, item):
        country = Country(**item)
        with Session(self.engine) as session:
            #session.expire_on_commit = False
            session.add(country)
            session.commit()

    def create_countries(self, items:list[Any]):
        """
        Tarea: revisar como hacer el bulk insert (todo en un linea)
        Create a list of countries"""
        countries = [Country(**item)  for item in items]
        # se inicia la transaccion
        with Session(self.engine) as session:
            session.add_all(countries)
            # se cierra la transaccion
            session.commit()
        return country

    def countries_by_continent(self, continent:str):
        query = f"""
        select * from country where continent ~* '{continent}'
        """
        df  = self.session.retrieve(query)
        return df

    def countries_by_continent_fn(self, continent:str):
        statement = select(Country).where(
            Country.continent.lower() == continent.lower())
        df  = self.session.retrieve(statement)
        return df

    def all_countries(self):
        stmt = select(Country)
        df = self.session.get(stmt)
        return df

    @property
    def engine(self):
        return self.session.db_engine


if __name__=="__main__":
    params = {
        "dbuser":"david",
        "dbpass":"oxido",
        "dbhost":"localhost",
        "dbport":5432,
        "dbname":"biblioteca_orm"
    }
    conn = BibliotecaSession(**params)
    manager = ManagerBiblioteca(session=conn)
    chile = {"name": "Chile", "continent": "América"}
    instance = manager.create_country(chile)
    print(instance, instance.id)

    countries = manager.countries_by_continent("América")
    print(countries)
