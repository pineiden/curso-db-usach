from sqlmodel import Field, SQLModel


class Style(SQLModel, table=True):
    id: int = Field(default=None, primary_key=True)
    name: str
    description: str


    def __repr__(self):
        return f"Style({self.name})"

    def __str__(self):
        return f"Style({self.name})"

    

