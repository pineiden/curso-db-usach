from sqlmodel import Field, SQLModel, Relationship


class Country(SQLModel, table=True):
    id: int = Field(default=None, primary_key=True)
    name: str
    continent: str

    books: list["Book"] = Relationship(back_populates='country')

    def __repr__(self):
        return f"Country({self.name}, {self.continent})"

    def __str__(self):
        return f"Country({self.name}, {self.continent})"

    
