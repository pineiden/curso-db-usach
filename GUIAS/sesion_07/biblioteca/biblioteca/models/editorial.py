from sqlmodel import Field, SQLModel

class Editorial(SQLModel, table=True):
    id: int = Field(default=None, primary_key=True)
    name: str
    web: str
    # foreign key from Country
    country_id: int|None = Field(
        default=None, 
        foreign_key="country.id")


    def __repr__(self):
        return f"Editorial({self.name})"

    def __str__(self):
        return f"Editorial({self.name})"

    


