from sqlmodel import Field, SQLModel, Relationship
from .country import Country

class Book(SQLModel, table=True):
    

    id: int = Field(default=None, primary_key=True)
    name: str
    pages: int
    start_year: int
    end_year: int 
    edition_year: int
    country_id: int|None = Field(
        default=None, 
        foreign_key="country.id")

    
    country: Country|None  = Relationship(back_populates="books") 

    editorial_id: int|None = Field(
        default=None,
        foreign_key="editorial.id"
    ) # Foreign key
    style_id: int|None = Field(
        default=None,
        foreign_key="style.id"
    )

    def __repr__(self):
        return f"Book({self.name})"

    def __str__(self):
        return f"Book({self.name})"
