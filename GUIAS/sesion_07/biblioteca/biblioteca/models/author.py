from sqlmodel import Field, SQLModel


class Author(SQLModel, table=True):
    id: int = Field(default=None, primary_key=True)
    name: str
    birth_year: int 
    country_id: int|None = Field(
        default=None, 
        foreign_key="country.id")

    def __repr__(self):
        return f"Author({self.name})"

    def __str__(self):
        return f"Author({self.name})"
