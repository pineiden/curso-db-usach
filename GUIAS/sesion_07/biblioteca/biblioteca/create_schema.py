from sqlmodel import SQLModel
from models import *
from connection import BibliotecaSession

def create_db_and_tables(engine):
    SQLModel.metadata.create_all(engine)


if __name__=="__main__":
    params = {
        "dbuser":"david",
        "dbpass":"oxido",
        "dbhost":"localhost",
        "dbport":5432,
        "dbname":"biblioteca_orm"
    }
    conn = BibliotecaSession(**params)

    create_db_and_tables(conn.db_engine)

    query_schema = """
SELECT table_name
FROM information_schema.tables
WHERE table_schema='public'
AND table_type='BASE TABLE';
"""
    df = conn.retrieve(query_schema)

    print("Conexión", conn)

    print(df)
