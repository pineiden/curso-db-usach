from django.contrib.gis.db import models
# Create your models here.

class Device(models.Model):
    CHOICES = [
        (0,"DESKTOP"),
        (1,"SERVER"),
        (2,"SMARTPHONE"),
        (3,"RASPBERRY")
    ]
    nombre = models.CharField(max_length=64)
    type = models.IntegerField(
        choices=CHOICES,
        default=0)
    did = models.IntegerField()
    position = models.PointField()
    date = models.DateTimeField(auto_now=True)

    class Meta:
        verbose_name="Device"
        verbose_name_plural="Devices"
        
