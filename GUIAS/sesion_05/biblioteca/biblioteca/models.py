import json
from sqlalchemy.orm import relationship
from sqlalchemy.orm import validates
from sqlalchemy import UniqueConstraint, Index
from sqlalchemy import Column, Integer, Text, String, DateTime, ForeignKey
from sqlalchemy import Boolean
from sqlalchemy.types import NUMERIC

from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy.ext.declarative import declared_attr
from sqlalchemy.ext.hybrid import hybrid_property

Base = declarative_base()


class Country(Base):
    __tablename__ = 'country'
    __table_args__ = {'schema': 'biblioteca'}

    id = Column(Integer, primary_key=True, autoincrement=True)
    name = Column(String(36), unique=True)
    continent = Column(String(36), nullable=False)

    def __repr__(self):
        return f"Country({self.id},{self.name},{self.continent})"

    def __str__(self):
        return f"Country({self.id},{self.name},{self.continent})"


class Style(Base):
    __tablename__ = 'style'
    __table_args__ = {'schema': 'biblioteca'}

    id = Column(Integer, primary_key=True, autoincrement=True)
    name = Column(String(36), unique=True)
    description = Column(String(256))

    def __repr__(self):
        return f"Style({self.id},{self.name})"

    def __str__(self):
        return f"Style({self.id},{self.name})"


class Editorial(Base):
    __tablename__ = 'editorial'
    __table_args__ = {'schema': 'biblioteca'}

    id = Column(Integer, primary_key=True, autoincrement=True)
    name = Column(String(36), unique=True)
    web = Column(String(512))
    country_id = Column(Integer, ForeignKey('biblioteca.country.id'))

    country = relationship('Country', back_populates='editorials')

    def __repr__(self):
        return f"Editorial({self.id},{self.name})"

    def __str__(self):
        return f"Editorial({self.id},{self.name})"


class Author(Base):
    __tablename__ = 'author'
    __table_args__ = {'schema': 'biblioteca'}

    id = Column(Integer, primary_key=True, autoincrement=True)
    name = Column(String(36), unique=True)
    birth_year = Column(Integer)

    country_id = Column(Integer, ForeignKey('biblioteca.country.id'))

    country = relationship('Country', back_populates='authors')

    def __repr__(self):
        return f"Author({self.id},{self.name}, {self.year})"

    def __str__(self):
        return f"Author({self.id},{self.name})"


class Book(Base):
    __tablename__ = 'book'
    __table_args__ = {'schema': 'biblioteca'}

    id = Column(Integer, primary_key=True, autoincrement=True)
    name = Column(String(36), unique=True)
    pages = Column(Integer, nullable=False)
    start_year = Column(Integer)
    end_year = Column(Integer, nullable=False)
    edicion_year = Column(Integer, nullable=False)

    country_id = Column(Integer, ForeignKey('biblioteca.country.id'))
    country = relationship('Country', back_populates='books')

    editorial_id = Column(Integer, ForeignKey('biblioteca.editorial.id'))
    editorial = relationship('Editorial', back_populates='books')

    style_id = Column(Integer, ForeignKey('biblioteca.style.id'))
    style = relationship('Style', back_populates='books')

    def __repr__(self):
        return f"Book({self.id},{self.name}, {self.edicion_year})"

    def __str__(self):
        return f"Book({self.id},{self.name})"


class BookAuthor(Base):
    __tablename__ = 'book_author'
    __table_args__ = {'schema': 'biblioteca'}

    id = Column(Integer, primary_key=True, autoincrement=True)

    author_id = Column(Integer, ForeignKey('biblioteca.country.id'))
    author = relationship('Author', back_populates='book_authors')

    book_id = Column(Integer, ForeignKey('biblioteca.editorial.id'))
    book = relationship('Book', back_populates='book_authors')

    def __repr__(self):
        return f"BookAuthor({self.id},{self.author_id}, {self.book_id})"

    def __str__(self):
        return f"BookAuthor({self.id},{self.author_id}, {self.book_id})"
