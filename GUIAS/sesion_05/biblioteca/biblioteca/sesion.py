from sqlalchemy.orm import sessionmaker
from sqlalchemy import create_engine
from sqlalchemy.sql import text


class BibliotecaSession:
    def __init__(self, *args, **kwargs):
        self.db_engine = 'postgresql://{dbuser}:{dbpass}@{dbhost}:{dbport}/{dbname}'.format(
            **kwargs)
        self.engine = create_engine(self.db_engine)
        self.connection = self.engine.connect()
        self.session = sessionmaker(bind=self.engine)()
        self.data = kwargs

    def get_session(self):
        return self.session

    def run_sql(self, sql):
        if isinstance(sql, str):
            sql = text(sql)
        self.connection.execute(sql)
