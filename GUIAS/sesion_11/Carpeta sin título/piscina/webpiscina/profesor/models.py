from django.db import models
from django.contrib.auth.models import User
from datetime import datetime, date
from django.utils.translation import gettext as _
import unicodedata
import string
from django.core.exceptions import ValidationError
from persona.models import Persona

def clean_string(text):
    return "".join([c for c in unicodedata.normalize('NFD',text) if c in string.ascii_letters])



def create_username(data):
    fname = clean_string(data['first_name'])
    lname = clean_string(data['last_name'])
    username = f"{fname}_{lname}".lower()
    return username


def validate_name(user):
    username = f"{clean_string(user.first_name)}_{clean_string(user.last_name)}".lower()
    if not username==user.username:
        raise ValidationError(_(f"""{username} not valid for profesor,
        change to first_name_last_name"""))

# Create your models here.
class Profesor(Persona):
    CATEGORIAS =  [
        ("aux","Auxiliar"),
        ("part_time","Part Time"),
        ("plt","Planta")
    ]
    es_profesor = models.BooleanField(default=True)
    categoria = models.CharField(
        max_length=64, default="aux", blank=False,
        choices=CATEGORIAS)
    """
    1)  profesor

    """

    def __repr__(self):
        return f"Profesor({self.nombre})"


    @property
    def info(self):
        return {
            "nombre":self.nombre,
            "email":self.user.email,
            "active": self.user.is_active
            
        }


    class Meta:
        verbose_name_plural = _("Profesores")


    def save(self, *args, **kwargs): 
        # Normalizar y convertir a minúsculas antes de guardar 
        validate_name(self.user)
        super().save(*args, **kwargs)

    @classmethod
    def create(cls, **items):
        username = create_username(items)
        user, created = User.objects.get_or_create(
            username=username,
            first_name=items["first_name"], 
            last_name=items["last_name"])
        if created:
            user.save()
        persona = Profesor(
            user=user, 
            birth_date=items["birth_date"], 
            es_profesor=True)
        return persona
