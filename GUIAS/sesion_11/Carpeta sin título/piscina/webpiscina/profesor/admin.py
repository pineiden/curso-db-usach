from django.contrib import admin
from .models import Profesor

# Register your models here.

class ProfesorAdmin(admin.ModelAdmin):
    list_display = ("user", "es_profesor")

admin.site.register(
    Profesor, 
    ProfesorAdmin)

# Register your models here.
