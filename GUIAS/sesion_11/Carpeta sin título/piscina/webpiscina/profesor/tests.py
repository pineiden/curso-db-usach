from django.test import TestCase
from .models import Profesor
# Create your tests here.
from datetime import date
from django.contrib.auth.models import User
import unicodedata
import string

def clean_string(text):
    return "".join([c for c in unicodedata.normalize('NFD',text) if c in string.ascii_letters])

class ProfesorTestCase(TestCase):
    """
    - Profesores tengan username=nombreapellido
    - mayor de edad (>18)

    """
    def setUp(self):
        userfields = {"first_name", "last_name"}
        users = {
            "juanperez":{
                "first_name":"Juan", 
                "last_name":"Pérez",
                "birthdate": date(1980,2,2)
            },
            "mariazapatero":{
                "first_name":"Maria", 
                "last_name":"Zapatero",
                "birthdate": date(2005,10,10)
            },
            "estebanrozas":{
                "first_name":"Esteban", 
                "last_name":"Rozas",
                "birthdate": date(1999,3,1)
            }
        }
        userslist = []
        for username, data in users.items():
            item = {
                "username": username, 
                **{f:data[f] for f in userfields}
            }
            user = User(**item)
            user.save()
            userslist.append((user, data["birthdate"]))

        self.users = userslist

    def test_username_equal_names(self):
        for user, bdt in self.users:
            username = clean_string(f"{user.first_name}{user.last_name}")
            self.assertEqual(
                user.username,
                username.lower()
            )
            profesor = Profesor(birth_date=bdt, user=user)
            profesor.save()

        profesores = [p for p in Profesor.objects.all()]
        self.assertEqual(len(profesores), 3)
