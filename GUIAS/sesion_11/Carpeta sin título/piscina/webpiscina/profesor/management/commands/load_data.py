from django.core.management.base import BaseCommand, CommandError
from profesor.models import Profesor, clean_string
from django.contrib.auth.models import User
from pathlib import Path
from rich import print
import csv


class Command(BaseCommand):
    help = "Load data for profesor table"
    
    def add_arguments(self, parser):
        parser.add_argument("path", type=Path)


    def handle(self, *args, **options):
        print("Comando para crear profesores")
        print(args, options)
        path = options["path"]
        if path.exists():
            with path.open() as f:
                reader = csv.DictReader(f)
                for row in reader:
                    persona = Profesor.create(**row)
                    persona.save()
        else:
            print(f"Ruta a {path} no existe")
            
