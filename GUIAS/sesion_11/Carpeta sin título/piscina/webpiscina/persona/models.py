from django.db import models
from django.contrib.auth.models import User

# Create your models here.
class Persona(models.Model):
    birth_date = models.DateField(
        auto_now=False,
        unique=False,
        blank=False
    )
    user = models.ForeignKey(
        User, 
        on_delete=models.PROTECT)


    class Meta:
        abstract = True



    @property
    def edad(self):
        return int((date.today()-self.birth_date).days/365.2425)

    @property
    def nombre(self):
        
        return f"{self.user.first_name} {self.user.last_name}"

    def __str__(self):
        return f"{self.nombre}"


