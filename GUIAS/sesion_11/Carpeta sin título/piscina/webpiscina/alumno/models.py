from django.db import models
from django.contrib.auth.models import User
from datetime import date, datetime
from django.utils.translation import gettext as _
from persona.models import Persona


# Create your models here.
class Alumno(Persona):
    es_alumno = models.BooleanField(
        default=False)
    """
    1) alumno no puede ser profesor

    """
    def __repr__(self):
        return f"Alumno({self.nombre})"

    @property
    def info(self):
        return {
            "nombre":self.nombre,
            "email":self.user.email,
            "active": self.user.is_active
            
        }


    class Meta:
        verbose_name_plural = _("Alumnos")
