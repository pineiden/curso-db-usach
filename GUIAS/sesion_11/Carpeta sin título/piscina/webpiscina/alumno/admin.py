from django.contrib import admin
from .models import Alumno

# Register your models here.

class AlumnoAdmin(admin.ModelAdmin):
    list_display = ("user", "es_alumno")

admin.site.register(
    Alumno, 
    AlumnoAdmin)
