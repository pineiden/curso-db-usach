from .models import Alumno
from django.contrib.auth.models import User
from django.urls import path, include
from rest_framework import routers, serializers, viewsets


class UserSerializer(serializers.HyperlinkedModelSerializer):
    class Meta:
        model = User
        fields = [
            "id",
            "username", 
            "first_name", 
            "last_name", 
            "email"]


class AlumnoSerializer(serializers.HyperlinkedModelSerializer):
    user = UserSerializer()

    class Meta:
        model = Alumno
        fields = ["id","birth_date", "es_alumno", "user"]

        
class UserViewSet(viewsets.ModelViewSet):
    queryset = User.objects.all()
    serializer_class = UserSerializer
    
class AlumnosViewSet(viewsets.ModelViewSet):
    queryset = Alumno.objects.filter(es_alumno=True)
    serializer_class = AlumnoSerializer


router = routers.DefaultRouter()

router.register(r'user', UserViewSet)
router.register(r'alumno', AlumnosViewSet)


alumno_urls = router.urls
