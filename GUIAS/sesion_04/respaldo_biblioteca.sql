--
-- PostgreSQL database dump
--

-- Dumped from database version 14.13 (Ubuntu 14.13-0ubuntu0.22.04.1)
-- Dumped by pg_dump version 14.13 (Ubuntu 14.13-0ubuntu0.22.04.1)

SET statement_timeout = 0;
SET lock_timeout = 0;
SET idle_in_transaction_session_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SELECT pg_catalog.set_config('search_path', '', false);
SET check_function_bodies = false;
SET xmloption = content;
SET client_min_messages = warning;
SET row_security = off;

SET default_tablespace = '';

SET default_table_access_method = heap;

--
-- Name: author; Type: TABLE; Schema: public; Owner: david
--

CREATE TABLE public.author (
    id integer NOT NULL,
    name character varying(50),
    birth_year integer
);


ALTER TABLE public.author OWNER TO david;

--
-- Name: author_id_seq; Type: SEQUENCE; Schema: public; Owner: david
--

CREATE SEQUENCE public.author_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.author_id_seq OWNER TO david;

--
-- Name: author_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: david
--

ALTER SEQUENCE public.author_id_seq OWNED BY public.author.id;


--
-- Name: book_author; Type: TABLE; Schema: public; Owner: david
--

CREATE TABLE public.book_author (
    id integer NOT NULL,
    book_id integer,
    author_id integer
);


ALTER TABLE public.book_author OWNER TO david;

--
-- Name: book_author_id_seq; Type: SEQUENCE; Schema: public; Owner: david
--

CREATE SEQUENCE public.book_author_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.book_author_id_seq OWNER TO david;

--
-- Name: book_author_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: david
--

ALTER SEQUENCE public.book_author_id_seq OWNED BY public.book_author.id;


--
-- Name: books; Type: TABLE; Schema: public; Owner: david
--

CREATE TABLE public.books (
    id integer NOT NULL,
    name character varying(255),
    pages integer NOT NULL,
    start_year integer,
    end_year integer,
    country_id integer,
    editorial_id integer,
    edition integer,
    style_id integer,
    CONSTRAINT book_check CHECK ((end_year >= start_year)),
    CONSTRAINT pages_check CHECK ((pages > 0))
);


ALTER TABLE public.books OWNER TO david;

--
-- Name: books_id_seq; Type: SEQUENCE; Schema: public; Owner: david
--

CREATE SEQUENCE public.books_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.books_id_seq OWNER TO david;

--
-- Name: books_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: david
--

ALTER SEQUENCE public.books_id_seq OWNED BY public.books.id;


--
-- Name: country; Type: TABLE; Schema: public; Owner: david
--

CREATE TABLE public.country (
    id integer NOT NULL,
    name character varying(50),
    flag text,
    continent character varying(50),
    main_city character varying(50)
);


ALTER TABLE public.country OWNER TO david;

--
-- Name: country_id_seq; Type: SEQUENCE; Schema: public; Owner: david
--

CREATE SEQUENCE public.country_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.country_id_seq OWNER TO david;

--
-- Name: country_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: david
--

ALTER SEQUENCE public.country_id_seq OWNED BY public.country.id;


--
-- Name: editorial; Type: TABLE; Schema: public; Owner: david
--

CREATE TABLE public.editorial (
    id integer NOT NULL,
    name character varying(50),
    web character varying(255),
    country_id integer
);


ALTER TABLE public.editorial OWNER TO david;

--
-- Name: editorial_id_seq; Type: SEQUENCE; Schema: public; Owner: david
--

CREATE SEQUENCE public.editorial_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.editorial_id_seq OWNER TO david;

--
-- Name: editorial_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: david
--

ALTER SEQUENCE public.editorial_id_seq OWNED BY public.editorial.id;


--
-- Name: style; Type: TABLE; Schema: public; Owner: david
--

CREATE TABLE public.style (
    id integer NOT NULL,
    name character varying(50),
    description text
);


ALTER TABLE public.style OWNER TO david;

--
-- Name: style_id_seq; Type: SEQUENCE; Schema: public; Owner: david
--

CREATE SEQUENCE public.style_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.style_id_seq OWNER TO david;

--
-- Name: style_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: david
--

ALTER SEQUENCE public.style_id_seq OWNED BY public.style.id;


--
-- Name: author id; Type: DEFAULT; Schema: public; Owner: david
--

ALTER TABLE ONLY public.author ALTER COLUMN id SET DEFAULT nextval('public.author_id_seq'::regclass);


--
-- Name: book_author id; Type: DEFAULT; Schema: public; Owner: david
--

ALTER TABLE ONLY public.book_author ALTER COLUMN id SET DEFAULT nextval('public.book_author_id_seq'::regclass);


--
-- Name: books id; Type: DEFAULT; Schema: public; Owner: david
--

ALTER TABLE ONLY public.books ALTER COLUMN id SET DEFAULT nextval('public.books_id_seq'::regclass);


--
-- Name: country id; Type: DEFAULT; Schema: public; Owner: david
--

ALTER TABLE ONLY public.country ALTER COLUMN id SET DEFAULT nextval('public.country_id_seq'::regclass);


--
-- Name: editorial id; Type: DEFAULT; Schema: public; Owner: david
--

ALTER TABLE ONLY public.editorial ALTER COLUMN id SET DEFAULT nextval('public.editorial_id_seq'::regclass);


--
-- Name: style id; Type: DEFAULT; Schema: public; Owner: david
--

ALTER TABLE ONLY public.style ALTER COLUMN id SET DEFAULT nextval('public.style_id_seq'::regclass);


--
-- Data for Name: author; Type: TABLE DATA; Schema: public; Owner: david
--

COPY public.author (id, name, birth_year) FROM stdin;
1	Gabriel García Marquez	1927
2	JK Rowling	1965
3	JRR Tolkien	1892
4	Julio Vierne	1828
5	Julio Cortazar	1914
6	Alicia Morel	1921
7	Marcela Paz	1902
8	Manuel Rojas	1896
9	Liu Cixin	1963
10	Baldomero Lillo	1867
11	Pablo Neruda	1904
12	Nicanor Parra	1914
13	Anónimo	\N
14	León Tolstoi	1828
15	Alejandro Dumas	1802
\.


--
-- Data for Name: book_author; Type: TABLE DATA; Schema: public; Owner: david
--

COPY public.book_author (id, book_id, author_id) FROM stdin;
1	1	1
2	2	2
3	3	3
4	4	4
6	6	6
5	5	5
7	6	7
8	7	8
9	8	9
10	9	1
11	10	4
12	11	5
13	12	10
14	13	10
15	14	11
16	15	11
17	16	12
18	17	13
19	18	14
20	19	15
\.


--
-- Data for Name: books; Type: TABLE DATA; Schema: public; Owner: david
--

COPY public.books (id, name, pages, start_year, end_year, country_id, editorial_id, edition, style_id) FROM stdin;
1	Cien años de soledad	471	\N	1982	1	6	2003	2
2	Harry Potter	3792	\N	1997	2	7	2020	2
3	El señor de los anillos, La comunidad del anillo	1392	\N	1954	2	8	2016	2
4	Miguel Strogoff	382	\N	1876	3	9	2018	2
5	Rayuela	471	\N	1963	5	10	2013	2
6	Perico trepa por Chile	471	\N	1978	4	11	2000	2
7	Hijo de Ladrón	471	\N	1951	4	12	2013	2
8	El problema de los tres cuerpos I	471	\N	2006	8	13	2021	2
9	El coronel no tiene quien le escriba	471	\N	1961	1	14	2003	2
10	La vuelta al mundo en 80 días	471	\N	1872	3	15	2002	2
11	La vuelta al día en 80 mundos	471	\N	1967	5	16	2010	2
12	Sub Terra	471	\N	1904	4	17	2014	2
13	Sub Sole	471	\N	1907	4	17	2018	2
14	20 poemas de amor y una canción desesperada	471	\N	1924	4	18	2009	1
15	Canto General	471	\N	1950	4	19	2018	1
16	Poemas y Antipoemas	471	\N	1954	4	20	2001	1
17	Mil y una Noches	471	\N	\N	6	21	2021	2
18	Guerra y Paz	471	\N	1965	7	22	2015	2
19	El conde de montecristo	1448	1884	1885	3	23	2016	2
\.


--
-- Data for Name: country; Type: TABLE DATA; Schema: public; Owner: david
--

COPY public.country (id, name, flag, continent, main_city) FROM stdin;
1	colombia	https://commons.wikimedia.org/wiki/File:Flag_of_Colombia.svg#/media/Archivo:Flag_of_Colombia.svg	América	Bogotá
2	reino Unido	https://commons.wikimedia.org/wiki/File:Flag_of_the_United_Kingdom_(3-5).svg#/media/Archivo:Flag_of_the_United_Kingdom_(3-5).svg	Europa	Londres
3	francia	https://commons.wikimedia.org/wiki/File:Flag_of_France_(1794%E2%80%931815,_1830%E2%80%931974).svg#/media/Archivo:Flag_of_France_(1794–1815,_1830–1974).svg	Europa	París
4	chile	https://commons.wikimedia.org/wiki/File:Flag_of_Chile.svg#/media/Archivo:Flag_of_Chile.svg	América	Santiago
5	argentina	https://commons.wikimedia.org/wiki/File:Flag_of_Argentina.svg#/media/Archivo:Flag_of_Argentina.svg	América	Buenos Aires
6	arabia	https://commons.wikimedia.org/wiki/File:Arabian_Peninsula_dust_SeaWiFS.jpg#/media/Archivo:Arabian_Peninsula_dust_SeaWiFS.jpg	Asia	Riad
7	rusia	https://commons.wikimedia.org/wiki/File:Flag_of_Russia.svg#/media/Archivo:Flag_of_Russia.svg	Europa, Asia	Moscú
8	china	https://commons.wikimedia.org/wiki/File:Flag_of_the_People%27s_Republic_of_China.svg#/media/Archivo:Flag_of_the_People's_Republic_of_China.svg	Asia	Pekín
9	españa	https://commons.wikimedia.org/wiki/File:Bandera_de_Espa%C3%B1a.svg#/media/Archivo:Bandera_de_España.svg	Europa	Madrid
\.


--
-- Data for Name: editorial; Type: TABLE DATA; Schema: public; Owner: david
--

COPY public.editorial (id, name, web, country_id) FROM stdin;
6	Contemporanea	https://contemporanea-ediciones.com.ar/	5
7	Salamandra	https://www.penguinlibros.com/es/11942-salamandra	9
8	Booket	https://www.planeta.es/es/booket	9
9	Mestas Ediciones	https://mestasediciones.com/	9
10	Alfaguara	https://www.penguinlibros.com/es/11579-alfaguara	9
11	SM	https://www.tiendasm.cl/produccion/	4
12	ZigZag	https://www.zigzag.cl/	4
13	Nova	https://www.penguinlibros.com/es/11773-nova	9
14	Debolsillo	https://www.penguinlibros.com/es/11338-debolsillo	9
15	Anaya	https://www.grupoanaya.es/	9
16	RM	https://editorialrm.com/	9
17	Edición Digital	https://ediciondigital.cl/	4
18	Edaf	https://www.edaf.net/	9
19	Seix Barral	https://www.planetadelibros.com/editorial/seix-barral/9	9
20	Editorial Universitaria	https://www.universitaria.cl/	4
21	Longseller	https://longseller.com.ar/	5
22	Alianza Editorial	https://www.alianzaeditorial.es/	9
23	Akal	https://www.akal.com/	9
\.


--
-- Data for Name: style; Type: TABLE DATA; Schema: public; Owner: david
--

COPY public.style (id, name, description) FROM stdin;
1	poesía	La poesía es un género literario que se caracteriza por el uso de la expresión artística a través del lenguaje, la métrica y la rima. Los poetas utilizan la musicalidad y la metáfora para transmitir emociones, imágenes y significados profundos en un formato compacto y lírico.
2	narrativa	La narrativa es un género literario que se centra en la creación de historias y relatos a través de la palabra escrita. Incluye novelas, cuentos, epopeyas y cualquier forma de escritura que tenga una estructura narrativa. Los narradores pueden ser en tercera persona u omniscientes, primera persona o incluso múltiples voces narrativas.
3	filosofía	En el contexto de géneros literarios, la filosofía se refiere a la escritura filosófica que busca explorar cuestiones fundamentales sobre la existencia, la moral, el conocimiento y la realidad. Los textos filosóficos son discursivos y argumentativos, y los filósofos utilizan la escritura para analizar, debatir y proponer teorías sobre temas abstractos y conceptuales. Los géneros filosóficos incluyen ensayos filosóficos, diálogos socráticos y tratados filosóficos.
\.


--
-- Name: author_id_seq; Type: SEQUENCE SET; Schema: public; Owner: david
--

SELECT pg_catalog.setval('public.author_id_seq', 15, true);


--
-- Name: book_author_id_seq; Type: SEQUENCE SET; Schema: public; Owner: david
--

SELECT pg_catalog.setval('public.book_author_id_seq', 20, true);


--
-- Name: books_id_seq; Type: SEQUENCE SET; Schema: public; Owner: david
--

SELECT pg_catalog.setval('public.books_id_seq', 19, true);


--
-- Name: country_id_seq; Type: SEQUENCE SET; Schema: public; Owner: david
--

SELECT pg_catalog.setval('public.country_id_seq', 9, true);


--
-- Name: editorial_id_seq; Type: SEQUENCE SET; Schema: public; Owner: david
--

SELECT pg_catalog.setval('public.editorial_id_seq', 23, true);


--
-- Name: style_id_seq; Type: SEQUENCE SET; Schema: public; Owner: david
--

SELECT pg_catalog.setval('public.style_id_seq', 3, true);


--
-- Name: author author_pkey; Type: CONSTRAINT; Schema: public; Owner: david
--

ALTER TABLE ONLY public.author
    ADD CONSTRAINT author_pkey PRIMARY KEY (id);


--
-- Name: book_author book_author_pkey; Type: CONSTRAINT; Schema: public; Owner: david
--

ALTER TABLE ONLY public.book_author
    ADD CONSTRAINT book_author_pkey PRIMARY KEY (id);


--
-- Name: books books_pkey; Type: CONSTRAINT; Schema: public; Owner: david
--

ALTER TABLE ONLY public.books
    ADD CONSTRAINT books_pkey PRIMARY KEY (id);


--
-- Name: country country_pkey; Type: CONSTRAINT; Schema: public; Owner: david
--

ALTER TABLE ONLY public.country
    ADD CONSTRAINT country_pkey PRIMARY KEY (id);


--
-- Name: editorial editorial_pkey; Type: CONSTRAINT; Schema: public; Owner: david
--

ALTER TABLE ONLY public.editorial
    ADD CONSTRAINT editorial_pkey PRIMARY KEY (id);


--
-- Name: style style_pkey; Type: CONSTRAINT; Schema: public; Owner: david
--

ALTER TABLE ONLY public.style
    ADD CONSTRAINT style_pkey PRIMARY KEY (id);


--
-- Name: book_author fk_author; Type: FK CONSTRAINT; Schema: public; Owner: david
--

ALTER TABLE ONLY public.book_author
    ADD CONSTRAINT fk_author FOREIGN KEY (author_id) REFERENCES public.author(id);


--
-- Name: book_author fk_books; Type: FK CONSTRAINT; Schema: public; Owner: david
--

ALTER TABLE ONLY public.book_author
    ADD CONSTRAINT fk_books FOREIGN KEY (book_id) REFERENCES public.books(id);


--
-- Name: editorial fk_country; Type: FK CONSTRAINT; Schema: public; Owner: david
--

ALTER TABLE ONLY public.editorial
    ADD CONSTRAINT fk_country FOREIGN KEY (country_id) REFERENCES public.country(id);


--
-- Name: books fk_country; Type: FK CONSTRAINT; Schema: public; Owner: david
--

ALTER TABLE ONLY public.books
    ADD CONSTRAINT fk_country FOREIGN KEY (country_id) REFERENCES public.country(id);


--
-- Name: books fk_editorial; Type: FK CONSTRAINT; Schema: public; Owner: david
--

ALTER TABLE ONLY public.books
    ADD CONSTRAINT fk_editorial FOREIGN KEY (editorial_id) REFERENCES public.editorial(id);


--
-- Name: books fk_style; Type: FK CONSTRAINT; Schema: public; Owner: david
--

ALTER TABLE ONLY public.books
    ADD CONSTRAINT fk_style FOREIGN KEY (style_id) REFERENCES public.style(id);


--
-- PostgreSQL database dump complete
--

