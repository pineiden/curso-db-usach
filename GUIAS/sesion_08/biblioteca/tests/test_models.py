from biblioteca.models import (Base, Country, Style, Editorial, Author,
                               Book, BookAuthor)
from sqlalchemy import text
from biblioteca.sesion import BibliotecaSession
from sqlalchemy.orm import Session
from sqlalchemy.orm import declarative_base, relationship, sessionmaker
from sqlalchemy import select
import pytest

def create_session():
    try:
        params = {
            "dbname": "biblioteca",
            "dbuser": "david",
            "dbpass": "oxido",
            "dbhost": "localhost",
            "dbport": "5432"
        }
        # print("Session")
        # print(BibliotecaSession)
        bibcon = BibliotecaSession(**params)
        # print(bibcon)
        #result = bibcon.session.execute(text("select * from books"))
        # print(result)
        # for item in result:
        #    print(item)
        return bibcon
    except Exception as e:
        raise e


sesion = create_session()
session = Session(sesion.engine)

stmt = select(Book).join(Style).where(
    Book.edition >= 1955)

print("Books")
# transacciones -> sql
with Session(sesion.engine) as session:
    for row in session.execute(stmt):
        book = row[0]
        print(book, type(book), book.country)
        #book.country_id = 2
    # session.commit()

    editorial = session.get(Editorial, {"id": 2})
    print(editorial)



class TestModels:
    def setup_class(self):
        engine = create_session().engine
        Session = sessionmaker(bind=engine)
        Base.metadata.create_all(engine)
        self.session = Session()

    def test_country_name(self):
        country = Country(
            name="HOla"*30, 
            continent="America",
            flag="asdasdasd", 
            main_city="Urano")
        with pytest.raises(Exception) as excinfo:  
            self.session.add(country)
            self.session.commit()
        assert str(excinfo.value) == "Country with name so long"  


    def teardown_class(self):
        # volver todo a 0
        self.session.rollback()
        # cerrar
        self.session.close()
