from geopy.geocoders import Nominatim
import psycopg2
import csv
from pathlib import Path
import typer

app = typer.Typer()

@app.command()
def run(path:Path):
    print("Path ->", path)
    # Configura tu conexión a la base de datos
    conn = psycopg2.connect("dbname=datosgeograficos user=david password=oxido")
    cur = conn.cursor()


    # Inicializa el geocoder
    geolocator = Nominatim(user_agent="mi_geocoder")
    print(geolocator)

    with path.open() as f:
        reader = csv.DictReader(f)

        for item in reader:
            print("Reading",item)
            # Dirección a geocodificar
            direccion = item["Dirección"]

            # Geocodificación
            location = geolocator.geocode(direccion)
            if location:
                nombre = item["Biblioteca"]
                latitud = location.latitude
                longitud = location.longitude
                # Insertar en la tabla
                print(nombre, latitud, longitud)
                cur.execute("""
                    INSERT INTO bibliotecas (nombre, direccion, posicion_geografica)
                    VALUES (%s, %s, ST_SetSRID(ST_MakePoint(%s, %s), 4326))
                """, (nombre, direccion, longitud, latitud))
                conn.commit()

    cur.close()
    conn.close()


if __name__=="__main__":
    app()
